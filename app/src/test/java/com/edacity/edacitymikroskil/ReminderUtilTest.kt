package com.edacity.edacitymikroskil

import android.test.mock.MockContext
import com.edacity.edacitymikroskil.personal.reminder.ReminderAdapter
import com.edacity.edacitymikroskil.personal.reminder.create.ReminderUtil
import org.junit.Assert.*
import org.junit.Test
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*

class ReminderUtilTest {
    val reminderUtil = ReminderUtil()
    val PATTERN = "yyyy-MM-dd HH:mm"


    @Test
    fun convert_to_date_time() {
        val s = "2010-10-15 09:27"
        assertEquals(Date::class.java, reminderUtil.stringToDate(s, PATTERN)::class.java)
    }

    @Test
    fun test_date_date() {
        val s = "2010-10-15 09:27"
        val date = reminderUtil.stringToDate(s, PATTERN)
        assertEquals(15, date.date)
    }

    @Test
    fun test_date_month() {
        val s = "2010-10-15 09:27"
        val date = reminderUtil.stringToDate(s, PATTERN)
        assertEquals(10, date.month + 1)
    }

    @Test
    fun test_date_year() {
        val s = "2010-10-15 09:27"
        val date = reminderUtil.stringToDate(s, PATTERN)
        assertEquals(2010, date.year + 1900)
    }

    @Test
    fun assert_date_correct() {
        val date = Date(1111 - 1900,11 - 1,11, 11, 11)
        assertNotNull(reminderUtil.extractDate(date))
        assertEquals("1111-11-11", reminderUtil.extractDate(date))
    }

    @Test
    fun assert_time_11() {
        val date = Date(11,11,11, 11, 11)
        assertNotNull(reminderUtil.extractTime(date))
        assertEquals("11:11", reminderUtil.extractTime(date))
    }

    @Test
    fun assert_time_9() {
        val date = Date(11,11,11, 9, 9)
        assertNotNull(reminderUtil.extractTime(date))
        assertEquals("09:09", reminderUtil.extractTime(date))
    }

    @Test
    fun assert_time_to_double_digit() {
        assertEquals("01",reminderUtil.timeToDoubleDigit(1))
        assertEquals("02",reminderUtil.timeToDoubleDigit(2))
        assertEquals("03",reminderUtil.timeToDoubleDigit(3))
        assertEquals("04",reminderUtil.timeToDoubleDigit(4))
        assertEquals("05",reminderUtil.timeToDoubleDigit(5))
        assertEquals("01",reminderUtil.timeToDoubleDigit(1))
    }

    @Test
    fun combinator_test() {
        val s = "1111-11-11 11:11"
        val date = reminderUtil.stringToDate(s, PATTERN)
        assertEquals(11, date.date)
        assertEquals(11, date.month + 1)
        assertEquals(1111, date.year + 1900)
        assertEquals("1111-11-11", reminderUtil.extractDate(date))
    }

    @Test
    fun date_formater() {
        val calendar = Calendar.getInstance()
        val date = calendar.time
        date.date += 1
        assertNotEquals("", reminderUtil.extractDateTime(date))
    }

    @Test
    fun timestamp_to_time_1() {
        val s = "1111-11-11 11:11"
        assertEquals("11:11", reminderUtil.timestampToTime(s))
    }

    @Test
    fun timestamp_to_time_2() {
        val s = "1111-11-11 09:09"
        assertEquals("09:09", reminderUtil.timestampToTime(s))
    }

    @Test
    fun timestamp_to_time_3() {
        val s = "1111-11-11 01:00"
        assertEquals("01:00", reminderUtil.timestampToTime(s))
    }

    @Test
    fun timestamp_to_date_1() {
        val s = "1111-11-11 09:09"
        assertEquals("1111-11-11", reminderUtil.timestampToDate(s))
    }

    @Test
    fun timestamp_to_date_2() {
        val s = "2018-08-31 09:09"
        assertEquals("2018-08-31", reminderUtil.timestampToDate(s))
    }

    @Test
    fun string_date_to_list_int() {
        val s = "2018-08-31"
        assertNotNull(reminderUtil.dateStringToListInt(s))
        assertEquals(3, reminderUtil.dateStringToListInt(s).size)
    }

    @Test
    fun list_int_date_to_year_1() {
        val s = "2018-08-31"
        assertEquals(
                2018,
                reminderUtil.listToYear(reminderUtil.dateStringToListInt(s))
        )
    }

    @Test
    fun list_int_date_to_year_2() {
        val s = "2017-08-31"
        assertEquals(
                2017,
                reminderUtil.listToYear(reminderUtil.dateStringToListInt(s))
        )
    }

    @Test
    fun list_int_date_to_year_3() {
        val s = "2000-08-31"
        assertEquals(
                2000,
                reminderUtil.listToYear(reminderUtil.dateStringToListInt(s))
        )
    }

    @Test
    fun list_int_date_to_month_1() {
        val s = "2018-08-31"
        assertEquals(
                7,
                reminderUtil.listToMonth(reminderUtil.dateStringToListInt(s))
        )
    }

    @Test
    fun list_int_date_to_month_2() {
        val s = "2018-01-31"
        assertEquals(
                0,
                reminderUtil.listToMonth(reminderUtil.dateStringToListInt(s))
        )
    }

    @Test
    fun list_int_date_to_month_3() {
        val s = "2018-12-31"
        assertEquals(
                11,
                reminderUtil.listToMonth(reminderUtil.dateStringToListInt(s))
        )
    }

    @Test
    fun list_int_date_to_day_1() {
        val s = "2018-12-31"
        assertEquals(
                31,
                reminderUtil.listToDay(reminderUtil.dateStringToListInt(s))
        )
    }

    @Test
    fun list_int_date_to_day_2() {
        val s = "2018-12-10"
        assertEquals(
                10,
                reminderUtil.listToDay(reminderUtil.dateStringToListInt(s))
        )
    }

    @Test
    fun list_int_date_to_day_3() {
        val s = "2018-12-01"
        assertEquals(
                1,
                reminderUtil.listToDay(reminderUtil.dateStringToListInt(s))
        )
    }

    @Test
    fun string_time_to_list_int() {
        val s = "09:03"
        assertNotNull(reminderUtil.timeStringToListInt(s))
        assertEquals(2, reminderUtil.timeStringToListInt(s).size)
    }

    @Test
    fun list_int_time_to_hour_1() {
        val s = "09:09"
        assertEquals(
                9,
                reminderUtil.listToHour(reminderUtil.timeStringToListInt(s))
        )
    }

    @Test
    fun list_int_time_to_hour_2() {
        val s = "01:09"
        assertEquals(
                1,
                reminderUtil.listToHour(reminderUtil.timeStringToListInt(s))
        )
    }

    @Test
    fun list_int_time_to_hour_3() {
        val s = "17:09"
        assertEquals(
                17,
                reminderUtil.listToHour(reminderUtil.timeStringToListInt(s))
        )
    }

    @Test
    fun list_int_time_to_minute_1() {
        val s = "09:09"
        assertEquals(
                9,
                reminderUtil.listToMinute(reminderUtil.timeStringToListInt(s))
        )
    }

    @Test
    fun list_int_time_to_minute_2() {
        val s = "09:54"
        assertEquals(
                54,
                reminderUtil.listToMinute(reminderUtil.timeStringToListInt(s))
        )
    }

    @Test
    fun list_int_time_to_minute_3() {
        val s = "09:00"
        assertEquals(
                0,
                reminderUtil.listToMinute(reminderUtil.timeStringToListInt(s))
        )
    }
}