package com.edacity.edacitymikroskil

import android.os.CountDownTimer
import android.util.Log
import com.edacity.edacitymikroskil.model.youtube.YoutubeResult
import com.edacity.edacitymikroskil.service.MainServicePresenter
import com.google.android.exoplayer2.source.ConcatenatingMediaSource
import com.google.android.exoplayer2.source.ExtractorMediaSource
import io.reactivex.*
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.functions.Action
import io.reactivex.functions.Consumer
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import io.reactivex.subscribers.ResourceSubscriber
import org.junit.Test

import org.junit.Assert.*
import org.reactivestreams.Subscriber
import org.reactivestreams.Subscription
import java.util.concurrent.TimeUnit

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
class ExampleUnitTest {
    @Test
    fun addition_isCorrect() {
        assertEquals(4, 2 + 2)
    }

    @Test
    fun test() {
        val observable = Observable.create<Int> { subscriber ->
            subscriber.onNext(10)
            println("create")
            subscriber.onNext(20)
            subscriber.onComplete()
        }
        val observer = object : Observer<Int>{
            override fun onComplete() {
                print("complete")
            }

            override fun onSubscribe(d: Disposable) {
                print("subscribed")
            }

            override fun onNext(t: Int) {
                println(t)
            }

            override fun onError(e: Throwable) {
            }
        }
        observable.subscribe(observer)
    }

    @Test fun fun2() {
        val data = listOf<String>("denny", "exel", "william", "darwin")

        val observable = Observable.create<String> { emitter ->
            data.forEach { emitter.onNext(it) }
        }

        val observer = object : Observer<String> {
            override fun onComplete() {
                println("complete")
            }

            override fun onSubscribe(d: Disposable) {
                println("onsubscribe")
            }

            override fun onNext(t: String) {
                println(t)
            }

            override fun onError(e: Throwable) {
            }

        }

        observable
                .map {it.toUpperCase()}
                .flatMap { Observable.just(it, "qwe") }
                .subscribeWith(observer)


    }

    @Test fun funny() {
        val requestList = arrayOf(
                Observable.just(1),
                Observable.just(2),
                Observable.just(3),
                Observable.just(4),
                Observable.just(5),
                Observable.just(6),
                Observable.just(7),
                Observable.just(8),
                Observable.just(9),
                Observable.just(10)
        ).asIterable()

        requestList.forEach {
            it.subscribeWith(object : Observer<Int> {
                override fun onComplete() {
                }

                override fun onSubscribe(d: Disposable) {
                }

                override fun onNext(t: Int) {
                    print("this is $t")
                }

                override fun onError(e: Throwable) {
                }
            })
        }

    }

    @Test fun funny_2() {
        val requestList = arrayOf(
                Single.just(1),
                Single.just(2),
                Single.just(3),
                Single.just(4),
                Single.just(5),
                Single.just(6),
                Single.just(7),
                Single.just(8),
                Single.just(9),
                Single.just(10)
        ).asIterable()

        requestList.forEach {
            it.subscribeWith(object : DisposableSingleObserver<Int>() {
                override fun onSuccess(t: Int) {
                    println("Single $t")
                }

                override fun onError(e: Throwable) {
                }

            })
        }

    }

    @Test fun funny_3() {
        val requestList = arrayOf<Single<Int>>(
                Single.just(1),
                Single.just(2),
                Single.just(3),
                Single.just(4),
                Single.just(5),
                Single.just(6),
                Single.just(7),
                Single.just(8),
                Single.just(9),
                Single.just(10)
        ).asIterable()

        Observable.fromIterable(requestList).subscribe(object : DisposableObserver<Single<Int>>(){
            override fun onComplete() {
                println("completados")
            }

            override fun onNext(t: Single<Int>) {
                t.subscribeWith(object : DisposableSingleObserver<Int>(){
                    override fun onSuccess(t: Int) {
                        println("single $t")
                    }

                    override fun onError(e: Throwable) {
                    }

                    override fun onStart() {
                        super.onStart()
                    }
                })            }

            override fun onError(e: Throwable) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }
        })

    }

}
