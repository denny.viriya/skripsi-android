package com.edacity.edacitymikroskil.generic

import android.arch.lifecycle.ViewModel
import com.edacity.edacitymikroskil.io.Delegate
import com.edacity.edacitymikroskil.io.RestClient
import com.edacity.edacitymikroskil.model.paged.PagedTracks
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class GenericViewModel: ViewModel() {
    var genericPagedTracks by Delegate.InitMutableLiveData<PagedTracks>()

    fun getArtistTracks(token: String, artistId: Int) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.getArtistTracks(artistId).enqueue(object : Callback<PagedTracks> {
            override fun onFailure(call: Call<PagedTracks>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<PagedTracks>?, response: Response<PagedTracks>?) {
                if (response!!.isSuccessful) {
                    genericPagedTracks.value = response.body()
                }
            }

        })
    }

    fun getTagTracks(token: String, tagId: Int) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.getTagTracks(tagId).enqueue(object : Callback<PagedTracks> {
            override fun onFailure(call: Call<PagedTracks>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<PagedTracks>?, response: Response<PagedTracks>?) {
                if (response!!.isSuccessful) {
                    genericPagedTracks.value = response.body()
                }
            }

        })
    }
}