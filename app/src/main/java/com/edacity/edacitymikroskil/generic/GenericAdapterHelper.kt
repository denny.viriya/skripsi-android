package com.edacity.edacitymikroskil.generic

import android.net.Uri
import com.edacity.edacitymikroskil.R

interface GenericAdapterHelper {
    fun getURLForResource(resourceId: Int): String {
        return Uri
                .parse("android.resource://" + R::class.java.getPackage().name + "/" + resourceId)
                .toString()
    }
}