package com.edacity.edacitymikroskil.generic

import android.net.Uri
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.edacity.edacitymikroskil.R

abstract class GenericListRvAdapter : RecyclerView.Adapter<GenericListRvAdapter.GenericListViewHolder>(), GenericAdapterHelper {
    final override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericListViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.holder_list_generic, parent, false)

        return GenericListViewHolder(view)
    }

    class GenericListViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
       val listImage: AppCompatImageView = itemView.findViewById(R.id.list_image)
       val listTitle: AppCompatTextView =  itemView.findViewById(R.id.list_title)
       val listSubtitle: AppCompatTextView =  itemView.findViewById(R.id.list_subtitle)
        val listSubtitle2: AppCompatTextView = itemView.findViewById(R.id.list_subtitle2)
       val listMenu: AppCompatImageView = itemView.findViewById(R.id.list_menu)
        val container: ConstraintLayout = itemView.findViewById(R.id.container)
    }
}