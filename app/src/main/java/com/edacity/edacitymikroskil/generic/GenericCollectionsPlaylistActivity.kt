package com.edacity.edacitymikroskil.generic

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatTextView
import android.text.Html
import android.widget.Toast
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.main.BaseActivity
import com.edacity.edacitymikroskil.model.Track
import com.edacity.edacitymikroskil.model.paged.PagedTracks
import com.edacity.edacitymikroskil.playlist.detail.PlaylistDetailActivity
import com.edacity.edacitymikroskil.trackplayer.TrackPlayerActivity
import java.util.ArrayList

class GenericCollectionsPlaylistActivity : BaseActivity() {

    companion object {
        const val TARGET = "TARGET"
        const val TARGET_ARTIST = "TARGET_ARTIST"
        const val TARGET_TAG = "TARGET_TAG"
        const val ITEM_ID = "ITEM_ID"
        const val ITEM_NAME = "ITEM_NAME"
    }

    private lateinit var mTarget: String
    private var mId: Int = 0
    private lateinit var mName: String

    private lateinit var mGenericList: PagedTracks
    private lateinit var mSessionManager: SessionManager
    private lateinit var mViewModel: GenericViewModel

    private lateinit var mTitle: AppCompatTextView
    private lateinit var mSubtitle: AppCompatTextView
    private lateinit var mPlayButton: AppCompatButton
    private lateinit var mTracksListTextView: AppCompatTextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_generic_collections_playlist)

        initObject()
        retrieveIntentGenericData()
        initView()
        loadCollection(mTarget, mId)
        observeLiveData()
    }

    private fun initObject() {
        mViewModel = ViewModelProviders.of(this).get(GenericViewModel::class.java)
        mSessionManager = SessionManager(this)
    }

    private fun retrieveIntentGenericData(){
        mTarget = intent.getStringExtra(TARGET)
        mId = intent.getIntExtra(ITEM_ID, 0)
        mName = intent.getStringExtra(ITEM_NAME)
    }

    private fun initView() {
        mTitle = findViewById(R.id.detail_title)
        mSubtitle = findViewById(R.id.detail_subtitle)
        mTracksListTextView = findViewById(R.id.detail_tracks_list_tv)
        mPlayButton = findViewById(R.id.detail_play_button)
    }

    private fun observeLiveData() {
        mViewModel.genericPagedTracks.observe(this, Observer {
            mGenericList = it!!
            bindView(mGenericList)
            initListener()
        })
    }

    private fun loadCollection(target: String, mId: Int) {
        when (target) {
            TARGET_ARTIST -> {
                mViewModel.getArtistTracks(mSessionManager.accountToken.token, mId)
            }

            TARGET_TAG -> {
                mViewModel.getTagTracks(mSessionManager.accountToken.token, mId)
            }
        }
    }

    private fun bindView(pagedTracks: PagedTracks) {
        val trackSize = pagedTracks.trackList.size.toString()
        mTitle.text = mName
        mSubtitle.text = "contains: $trackSize tracks"
        mTracksListTextView.text = Html.fromHtml(constructStrings(pagedTracks.trackList))
    }

    private fun constructStrings(tracks: List<Track>) : String {

        var result = ""
        tracks.forEachIndexed { index, track ->
            when {
                (index < 9) -> {
                    result += "<b>${track.title}</b> <i>${track.artist.name}</i>"
                    if (index < tracks.size - 1) {
                        result += " | "
                    }
                }
            }
        }
        return result
    }

    private fun initListener() {
        mPlayButton.setOnClickListener {
            if (mGenericList.trackList.isEmpty()) {
                Toast.makeText(this, "You have no Tracks in this playlist!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val intent = Intent(this, TrackPlayerActivity::class.java).apply {
                putParcelableArrayListExtra(TrackPlayerActivity.PLAYLIST, mGenericList.trackList as ArrayList<Track>)
                putExtra(TrackPlayerActivity.PLAY_POSITION, 0)
            }
            this.startActivity(intent)
        }
    }

}
