package com.edacity.edacitymikroskil.generic

import android.net.Uri
import android.support.v7.widget.AppCompatImageButton
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.edacity.edacitymikroskil.R

open class GenericGridRvAdapter : RecyclerView.Adapter<GenericGridRvAdapter.GenericGridViewHolder>(), GenericAdapterHelper {
    final override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericGridViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.holder_grid_generic, parent, false)

        return GenericGridViewHolder(view)
    }

    override fun getItemCount(): Int {
        return 0
    }

    override fun onBindViewHolder(holder: GenericGridViewHolder, position: Int) {
    }

    class GenericGridViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val cardImage = itemView.findViewById<AppCompatImageView>(R.id.card_image)
        val cardTitle = itemView.findViewById<AppCompatTextView>(R.id.card_title)
        val cardSubtitle = itemView.findViewById<AppCompatTextView>(R.id.card_subtitle)
        val cardMenu = itemView.findViewById<AppCompatImageButton>(R.id.card_menu)
    }
}