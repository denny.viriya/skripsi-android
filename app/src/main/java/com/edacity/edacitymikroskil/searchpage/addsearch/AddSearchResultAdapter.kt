package com.edacity.edacitymikroskil.searchpage.addsearch

import android.annotation.SuppressLint
import android.widget.Toast
import com.edacity.edacitymikroskil.generic.GenericListRvAdapter
import com.edacity.edacitymikroskil.model.playlist.Playlist

class AddSearchResultAdapter(var dataset: List<Playlist>): GenericListRvAdapter(){

    lateinit var viewModel: AddSearchResultViewModel
    var token = ""
    var trackId = 0

    override fun getItemCount(): Int {
        return dataset.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: GenericListViewHolder, position: Int) {
        holder.listTitle.text = dataset[position].name
        holder.listSubtitle.text = "contains: ${dataset[position].tracks.size} tracks"

        holder.container.setOnClickListener {
            viewModel.createTrackInPlaylist(token, dataset[position].id, trackId)
        }
    }
}