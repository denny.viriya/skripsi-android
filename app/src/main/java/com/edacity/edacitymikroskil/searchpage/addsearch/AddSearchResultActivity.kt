package com.edacity.edacitymikroskil.searchpage.addsearch

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.main.BaseActivity
import com.edacity.edacitymikroskil.model.playlist.PagedPlaylist
import com.edacity.edacitymikroskil.model.playlist.Playlist
import com.edacity.edacitymikroskil.playlist.add.AddPlaylistActivity

class AddSearchResultActivity : BaseActivity() {

    private val TRACK_ID = "TRACK_ID"

    private lateinit var mSessionManager: SessionManager
    private lateinit var mViewModel: AddSearchResultViewModel
    private var mTrackId : Int = 0
    private lateinit var mPlaylistAdapter: AddSearchResultAdapter
    private var mPlaylists: PagedPlaylist = PagedPlaylist()

    private lateinit var mTitle: AppCompatTextView
    private lateinit var mButton: AppCompatButton
    private lateinit var mRecyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_search_result)
        initObject()
        initView()
        setupView()
        initListener()
        initObserver()
    }

    override fun onResume() {
        super.onResume()
        loadPlaylistData()
    }

    private fun initObject() {
        mViewModel = ViewModelProviders.of(this).get(AddSearchResultViewModel::class.java)
        mSessionManager = SessionManager(this)
        mTrackId = intent.getIntExtra(TRACK_ID, 0)

        mPlaylistAdapter = AddSearchResultAdapter(mPlaylists.playlists).apply {
            viewModel = mViewModel
            token = mSessionManager.accountToken.token
            trackId = mTrackId
        }
    }

    private fun initView() {
        mTitle = findViewById(R.id.layout_title)
        mButton = findViewById(R.id.select_playlist_button)
        mRecyclerView = findViewById(R.id.select_playlist_rv)
    }

    private fun setupView() {
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.adapter = mPlaylistAdapter
    }

    private fun initObserver() {
        mViewModel.getPlaylistLiveData!!.observe(this, Observer {
            mPlaylists = it!!
            mPlaylistAdapter.dataset = it.playlists
            mPlaylistAdapter.notifyDataSetChanged()
        })

        mViewModel.createTrackLiveData!!.observe(this, Observer {
            onBackPressed()
            Toast.makeText(this, "Successfully added track to playlist", Toast.LENGTH_SHORT).show()
        })
    }

    private fun initListener() {
        mButton.setOnClickListener {
            val intent = Intent(this, AddPlaylistActivity::class.java)
            startActivity(intent)
        }
    }

    private fun loadPlaylistData() {
        mViewModel.getPlaylists(mSessionManager.accountToken.token)
    }
}
