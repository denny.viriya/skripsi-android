package com.edacity.edacitymikroskil.searchpage


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.app.Fragment
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AutoCompleteTextView
import android.widget.ProgressBar

import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.model.search.SearchResult
import com.edacity.edacitymikroskil.searchpage.result.ArtistRvAdapter
import com.edacity.edacitymikroskil.searchpage.result.TagRvAdapter
import com.edacity.edacitymikroskil.searchpage.result.TrackRvAdapter


/**
 * A simple [Fragment] subclass.
 * Use the [SearchFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class SearchFragment : Fragment() {

    private lateinit var mViewModel: SearchViewModel
    private lateinit var mSessionManager: SessionManager
    private var mSearchResult: SearchResult = SearchResult()
    private var trackRvAdapter = TrackRvAdapter(listOf())
    private var artistRvAdapter = ArtistRvAdapter(listOf())
    private var tagRvAdapter = TagRvAdapter(listOf())

    private lateinit var mContainer: ViewGroup
    private lateinit var mSearchBar: AutoCompleteTextView
    private lateinit var mTrackTitle: AppCompatTextView
    private lateinit var mArtistTitle: AppCompatTextView
    private lateinit var mTagTitle: AppCompatTextView
    private lateinit var mTrackRv: RecyclerView
    private lateinit var mArtistRv: RecyclerView
    private lateinit var mTagRv: RecyclerView
    private lateinit var mSearchProgressBar: ProgressBar
    private lateinit var mSearchDivider: View


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObject()
    }

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_search, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view = view)
        setupRecyclerView()
        addSearchTextWatcher()
        addSearchOnFocusListener()
        initObserver()
    }

    private fun initObject() {
        mViewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)
        mSessionManager = SessionManager(this.context!!)
        trackRvAdapter.viewModel = mViewModel
        trackRvAdapter.token = mSessionManager.accountToken.token
    }

    private fun initView(view: View) {
        mSearchBar = view.findViewById(R.id.search_autocomplete_textview)
        mContainer = view.findViewById(R.id.container)
        mTrackTitle = view.findViewById(R.id.track_label)
        mArtistTitle = view.findViewById(R.id.artist_label)
        mTagTitle = view.findViewById(R.id.tag_label)
        mTrackRv = view.findViewById(R.id.track_rv)
        mArtistRv = view.findViewById(R.id.artist_rv)
        mTagRv = view.findViewById(R.id.tag_rv)
        mSearchProgressBar = view.findViewById(R.id.search_progress_bar)
        mSearchDivider = view.findViewById(R.id.search_divider)
    }

    private fun setupRecyclerView() {
        mTrackRv.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        mArtistRv.layoutManager =LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        mTagRv.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)

        mTrackRv.setHasFixedSize(true)
        mArtistRv.setHasFixedSize(true)
        mTagRv.setHasFixedSize(true)

        mTrackRv.adapter = trackRvAdapter
        mArtistRv.adapter = artistRvAdapter
        mTagRv.adapter = tagRvAdapter
    }

    private fun addSearchTextWatcher() {
        var timer : CountDownTimer? = null

        mSearchBar.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                timer?.cancel()
                if ((s?:"").isNotBlank()) {
                    val keyword = s!!.trimStart().trimEnd().toString()
                    timer = createTimer(600) {
                        showSearchLoadingProgressBar(true)
                        mViewModel.getSearchResult(
                                token = mSessionManager.accountToken.token,
                                keyword = keyword
                        )
                    }.start()
                } else {
                    mSearchProgressBar.visibility = View.GONE
                }
            }
        })
    }

    private fun addSearchOnFocusListener() {
        mSearchBar.setOnFocusChangeListener { v, hasFocus ->
            if (hasFocus) {
                mSearchDivider.visibility = View.VISIBLE
            } else {
                mSearchDivider.visibility = View.INVISIBLE
            }
        }
    }

    private fun createTimer(interval: Long, func: () -> Unit): CountDownTimer {
        val timer = object : CountDownTimer(interval, 10) {
            override fun onFinish() {
                func()
            }

            override fun onTick(millisUntilFinished: Long) {
            }

        }
        return timer
    }

    private fun initObserver() {
        mViewModel.searchResultLiveData!!.observe(this, Observer {
            mSearchResult = it!!

            mTrackTitle.visibility = View.VISIBLE
            trackRvAdapter.tracks = mSearchResult.trackList
            trackRvAdapter.notifyDataSetChanged()


            mArtistTitle.visibility = View.VISIBLE
            artistRvAdapter.artists = mSearchResult.artistList
            artistRvAdapter.notifyDataSetChanged()


            mTagTitle.visibility = View.VISIBLE
            tagRvAdapter.tags = mSearchResult.tags
            tagRvAdapter.notifyDataSetChanged()

            showSearchLoadingProgressBar(false)
        })
    }

    private fun showSearchLoadingProgressBar(value: Boolean) {
        if (value) {
            mSearchProgressBar.visibility = View.VISIBLE
        } else {
            mSearchProgressBar.visibility = View.INVISIBLE
        }
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment SearchFragment.
        */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() = SearchFragment()
    }
}
