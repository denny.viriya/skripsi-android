package com.edacity.edacitymikroskil.searchpage.result

import android.content.Intent
import android.text.Html
import android.view.ContextThemeWrapper
import android.view.View
import android.widget.PopupMenu
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.generic.GenericListRvAdapter
import com.edacity.edacitymikroskil.model.Track
import com.edacity.edacitymikroskil.searchpage.SearchViewModel
import com.edacity.edacitymikroskil.searchpage.addsearch.AddSearchResultActivity
import com.edacity.edacitymikroskil.trackplayer.TrackPlayerActivity
import java.util.ArrayList

class TrackRvAdapter(var tracks: List<Track>) : GenericListRvAdapter() {

    private val TRACK_ID = "TRACK_ID"

    lateinit var viewModel : SearchViewModel
    var token = ""

    override fun getItemCount(): Int {
        return tracks.size
    }

    override fun onBindViewHolder(holder: GenericListViewHolder, position: Int) {

        val themeWrapper = ContextThemeWrapper(holder.itemView.context, R.style.ListMenu)
        val popup = PopupMenu(themeWrapper, holder.listMenu);
        popup.setOnMenuItemClickListener {
            when (it!!.itemId) {
                R.id.add_to_playlist -> {
                    val intent = Intent(holder.itemView.context, AddSearchResultActivity::class.java)
                    intent.putExtra(TRACK_ID, tracks[position].id)
                    holder.itemView.context.startActivity(intent)
                    true
                }
                else -> false
            }
        }
        val inflater = popup.menuInflater;
        inflater.inflate(R.menu.generic_list_menu, popup.menu);

        holder.listImage.setImageResource(R.drawable.music_cover_1)
        holder.listTitle.text = tracks[position].title
        holder.listSubtitle.text = tracks[position].artist.name
        if (tracks[position].ratingPrediction != -1F) {
            holder.listSubtitle2.text = "Rating prediction : ${tracks[position].ratingPrediction}"
            holder.listSubtitle2.visibility = View.VISIBLE
        } else {
            holder.listSubtitle2.visibility = View.GONE
        }
        holder.listMenu.setImageResource(R.drawable.ic_playlist_add)

        holder.container.setOnClickListener {
            val intent = Intent(holder.itemView.context, TrackPlayerActivity::class.java).apply {
                putParcelableArrayListExtra(TrackPlayerActivity.PLAYLIST, tracks as ArrayList<Track>)
                putExtra(TrackPlayerActivity.PLAY_POSITION, position)
            }
            holder.itemView.context.startActivity(intent)
        }

        holder.listMenu.setOnClickListener {
            popup.show()
        }
    }
}