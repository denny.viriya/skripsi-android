package com.edacity.edacitymikroskil.searchpage.result

import android.annotation.SuppressLint
import android.content.Intent
import android.view.View
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.generic.GenericCollectionsPlaylistActivity
import com.edacity.edacitymikroskil.generic.GenericListRvAdapter
import com.edacity.edacitymikroskil.model.Artist
import com.squareup.picasso.Picasso

class ArtistRvAdapter(var artists: List<Artist>) : GenericListRvAdapter() {

    override fun getItemCount(): Int {
        return artists.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: GenericListViewHolder, position: Int) {
        Picasso.get().load(getURLForResource(R.drawable.music_cover_2)).fit().into(holder.listImage)
        holder.listTitle.text = artists[position].name
        holder.listSubtitle.text = "Artist Id: ${artists[position].id}"
        holder.listMenu.visibility = View.GONE

        holder.container.setOnClickListener {
            val intent = Intent(holder.itemView.context, GenericCollectionsPlaylistActivity::class.java)
            intent.apply {
                putExtra(GenericCollectionsPlaylistActivity.TARGET, GenericCollectionsPlaylistActivity.TARGET_ARTIST)
                putExtra(GenericCollectionsPlaylistActivity.ITEM_ID, artists[position].id)
                putExtra(GenericCollectionsPlaylistActivity.ITEM_NAME, artists[position].name)
            }
            holder.itemView.context.startActivity(intent)
        }
    }
}