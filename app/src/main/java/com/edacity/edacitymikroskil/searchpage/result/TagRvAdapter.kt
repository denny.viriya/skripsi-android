package com.edacity.edacitymikroskil.searchpage.result

import android.content.Intent
import android.support.v4.content.ContextCompat
import android.view.View
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.generic.GenericCollectionsPlaylistActivity
import com.edacity.edacitymikroskil.generic.GenericListRvAdapter
import com.edacity.edacitymikroskil.model.Tag
import com.edacity.edacitymikroskil.playlist.detail.PlaylistDetailActivity
import com.squareup.picasso.Picasso

class TagRvAdapter(var tags: List<Tag>) : GenericListRvAdapter() {

    override fun getItemCount(): Int {
        return tags.size
    }

    override fun onBindViewHolder(holder: GenericListViewHolder, position: Int) {
        Picasso.get().load(getURLForResource(R.drawable.music_cover_3)).fit().into(holder.listImage)
        holder.listTitle.text = tags[position].name
        holder.listSubtitle.text = "Tag Id: ${tags[position].id}"
        holder.listMenu.visibility = View.GONE

        holder.container.setOnClickListener {
            val intent = Intent(holder.itemView.context, GenericCollectionsPlaylistActivity::class.java)
            intent.apply {
                putExtra(GenericCollectionsPlaylistActivity.TARGET, GenericCollectionsPlaylistActivity.TARGET_TAG)
                putExtra(GenericCollectionsPlaylistActivity.ITEM_ID, tags[position].id)
                putExtra(GenericCollectionsPlaylistActivity.ITEM_NAME, tags[position].name)
            }
            holder.itemView.context.startActivity(intent)
        }
    }
}