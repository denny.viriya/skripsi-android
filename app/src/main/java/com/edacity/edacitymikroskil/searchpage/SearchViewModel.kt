package com.edacity.edacitymikroskil.searchpage

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.edacity.edacitymikroskil.io.RestClient
import com.edacity.edacitymikroskil.model.ResponseStatus
import com.edacity.edacitymikroskil.model.search.SearchResult
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class SearchViewModel: ViewModel() {

    var searchResultLiveData: MutableLiveData<SearchResult>? = null
        get() {
            if (field==null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    var searchErrorLiveData: MutableLiveData<ResponseStatus>? = null
        get() {
            if (field==null) {
                field = MutableLiveData()
            }
            return field
        }
        private set


    fun getSearchResult(token: String, keyword: String) {
        val apiInterface = RestClient.getApiInterface(token)
        val searchResultCallback = apiInterface.getSearchResult(keyword)

        searchResultCallback.enqueue(object : Callback<SearchResult> {
            override fun onFailure(call: Call<SearchResult>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<SearchResult>?, response: Response<SearchResult>?) {
                if (response!!.isSuccessful) {
                    searchResultLiveData!!.value = response.body()
                } else {
                    searchErrorLiveData!!.value = ResponseStatus(response.code(), response.message())
                }
            }
        })
    }
}