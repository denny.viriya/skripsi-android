package com.edacity.edacitymikroskil.searchpage.addsearch

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.edacity.edacitymikroskil.io.RestClient
import com.edacity.edacitymikroskil.model.ResponseStatus
import com.edacity.edacitymikroskil.model.playlist.PagedPlaylist
import com.edacity.edacitymikroskil.playlist.PlaylistViewModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddSearchResultViewModel: PlaylistViewModel() {

    var createTrackLiveData: MutableLiveData<Int>? = null
        get() {
            if (field == null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    var createTrackErrorLiveData: MutableLiveData<Int>? = null
        get() {
            if (field == null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    fun createTrackInPlaylist(token: String, playlistId: Int, trackId: Int) {
        val apiInterface = RestClient.getApiInterface(token)
        val body = composeHashMap("track_id", trackId)

        apiInterface.createTrackInPlaylist(playlistId, body).enqueue(object : Callback<Unit> {
            override fun onFailure(call: Call<Unit>?, t: Throwable?) {
                throw(t!!)
            }

            override fun onResponse(call: Call<Unit>?, response: Response<Unit>?) {
                if (response!!.isSuccessful) {
                    createTrackLiveData!!.value = playlistId
                } else {
                    createTrackErrorLiveData!!.value = playlistId
                }
            }

        })
    }

    private fun composeHashMap(key: String, value: Int): Map<String, Int> {
        val body = HashMap<String, Int>()
        body[key] = value
        return body
    }

}