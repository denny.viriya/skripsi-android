package com.edacity.edacitymikroskil.trackplayer

import android.annotation.SuppressLint
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.model.Tag

class TrackTaggedAdapter(var tags: List<Tag>) : RecyclerView.Adapter<TrackTaggedAdapter.TrackTaggedViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TrackTaggedViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.tag_holder, parent, false)
        return TrackTaggedViewHolder(view)
    }

    override fun getItemCount(): Int {
        return tags.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: TrackTaggedViewHolder, position: Int) {
        holder.tagName.text = tags[position].name
        holder.tagFrequency.text = "${tags[position].count}"
    }

    class TrackTaggedViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val tagName = itemView.findViewById<AppCompatTextView>(R.id.tag_name)
        val tagFrequency = itemView.findViewById<AppCompatTextView>(R.id.tag_frequency)
    }
}