package com.edacity.edacitymikroskil.trackplayer

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import com.edacity.edacitymikroskil.model.Track

class TrackPagerAdapter(var fragmentManager: FragmentManager, var dataset: List<Track>): FragmentStatePagerAdapter(fragmentManager) {
    override fun getItem(position: Int): Fragment {
        return TrackPagerFragment.newInstance(dataset[position])
    }

    override fun getCount(): Int {
        return dataset.size
    }
}