package com.edacity.edacitymikroskil.trackplayer

import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.*
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.model.Track
import jp.wasabeef.blurry.Blurry


class TrackPagerFragment : Fragment() {

    private val TRACK = "param1"

    private lateinit var mTrack: Track

    private lateinit var mTrackBackgroundImageView: AppCompatImageView
    private lateinit var mTrackImageView: AppCompatImageView
    private lateinit var mTrackTitle: AppCompatTextView
    private lateinit var mTrackSubtitle: AppCompatTextView
    private lateinit var mTagRecyclerView: RecyclerView
    private var mTaggedAdapter: TrackTaggedAdapter = TrackTaggedAdapter(emptyList())

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_player, container, false)
        initObject()

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view = view)
        setupView()
        reloadTaggedAdapter()
    }

    private fun initObject() {
        mTrack = arguments!!.getParcelable(TRACK)
    }

    private fun initView(view: View) {
        mTrackBackgroundImageView = view.findViewById(R.id.track_background_image)
        mTrackImageView = view.findViewById(R.id.track_image)
        mTrackTitle = view.findViewById(R.id.track_title)
        mTrackSubtitle = view.findViewById(R.id.track_subtitle)
        mTagRecyclerView = view.findViewById(R.id.tag_recyclerview)
    }

    private fun setupView() {
//        val bitmap = BitmapFactory
//                .decodeResource(
//                        this.resources,
//                        R.drawable.music_cover_1
//                )
//
//        Blurry
//                .with(this.context)
//                .color(R.color.colorAccent)
//                .from(bitmap)
//                .into(mTrackBackgroundImageView)

        mTrackImageView.setImageResource(R.drawable.music_cover_1)
        mTrackTitle.text = mTrack.title
        mTrackSubtitle.text = mTrack.artist.name

        mTagRecyclerView.setHasFixedSize(false)
        mTagRecyclerView.layoutManager = GridLayoutManager(
                this.context,
                1,
                LinearLayoutManager.HORIZONTAL,
                false
        )
        mTagRecyclerView.adapter = mTaggedAdapter
    }

    private fun reloadTaggedAdapter() {
        mTaggedAdapter.tags = mTrack.taggedList
        mTaggedAdapter.notifyDataSetChanged()
    }

    companion object {
        fun newInstance(track: Track) = TrackPagerFragment().apply {
            arguments = Bundle().apply {
                putParcelable(TRACK, track)
            }
        }
    }

}
