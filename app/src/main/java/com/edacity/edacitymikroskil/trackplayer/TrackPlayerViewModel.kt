package com.edacity.edacitymikroskil.trackplayer

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.edacity.edacitymikroskil.io.RestClient
import com.edacity.edacitymikroskil.model.ResponseStatus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TrackPlayerViewModel: ViewModel() {

    var playLiveData: MutableLiveData<ResponseStatus>? = null
        get() {
            if (field == null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    fun playTrack(token: String, trackId: Int) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.playTrack(trackId).enqueue(object : Callback<ResponseStatus> {
            override fun onFailure(call: Call<ResponseStatus>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<ResponseStatus>?, response: Response<ResponseStatus>?) {
                playLiveData!!.value = response!!.body()
            }
        })
    }

}