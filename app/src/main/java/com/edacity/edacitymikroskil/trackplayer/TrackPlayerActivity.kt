package com.edacity.edacitymikroskil.trackplayer

import android.app.ActivityManager
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.Bundle
import com.edacity.edacitymikroskil.R
import android.os.IBinder
import android.support.v4.view.ViewPager
import android.support.v7.widget.AppCompatTextView
import android.util.Log
import android.view.View
import android.widget.AutoCompleteTextView
import android.widget.ProgressBar
import android.widget.Toast
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.main.BaseActivity
import com.edacity.edacitymikroskil.main.MainActivity
import com.edacity.edacitymikroskil.model.Track
import com.edacity.edacitymikroskil.service.MainService
import com.google.android.exoplayer2.ui.PlayerControlView
import com.google.android.exoplayer2.util.Util
import java.util.ArrayList
import com.edacity.edacitymikroskil.service.MainService.LocalBinder
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.source.TrackGroupArray
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import io.reactivex.SingleObserver
import io.reactivex.disposables.Disposable

class TrackPlayerActivity : BaseActivity() {

    private lateinit var mSessionManager: SessionManager
    private lateinit var mViewModel: TrackPlayerViewModel
    private lateinit var mInitialIntentTrackList: List<Track>
    private var mInitialIntentPosition: Int = 0
    private var mInitialIntentPlayFrom: String = PLAY_FROM_APP
    private lateinit var mPager: ViewPager
    private lateinit var mPagerAdapter: TrackPagerAdapter
    private lateinit var mService: MainService
    private var mBound = false
    private lateinit var mServiceIntent: Intent
    private lateinit var mPlayerServiceConnection: ServiceConnection
    private lateinit var mPlayerControlView: PlayerControlView
    private lateinit var mProgressBar: ProgressBar
    private lateinit var mProgressText: AppCompatTextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_track_player)

        initObject()

        startServiceIfNotCreated(mServiceIntent)

        initializeViews()
        initObserver()
    }

    override fun onStart() {
        super.onStart()
        bindService(mServiceIntent, mPlayerServiceConnection, Context.BIND_AUTO_CREATE)
    }

    override fun onStop() {
        super.onStop()
        unbindService(mPlayerServiceConnection)
        mBound = false
    }

    override fun onBackPressed() {
        super.onBackPressed()
        if (isTaskRoot) {
            val mainActivityIntent = Intent(this, MainActivity::class.java)
            startActivity(mainActivityIntent)
        }
    }

    private fun initObject() {
        mViewModel = ViewModelProviders.of(this).get(TrackPlayerViewModel::class.java)
        mSessionManager = SessionManager(this)
        mInitialIntentTrackList = intent.getParcelableArrayListExtra(PLAYLIST)
        mInitialIntentPosition = intent.getIntExtra(PLAY_POSITION, 0)
        mInitialIntentPlayFrom = intent.getStringExtra(PLAY_FROM) ?: PLAY_FROM_APP
        mPagerAdapter = TrackPagerAdapter(
                fragmentManager = supportFragmentManager,
                dataset = mInitialIntentTrackList
        )
        mServiceIntent = createServiceIntent(mInitialIntentTrackList, mInitialIntentPosition)
        mPlayerServiceConnection = PlayerServiceConnection()
    }

    private fun createServiceIntent(tracks: List<Track>, position: Int): Intent {
        intent = Intent(this, MainService::class.java).apply {
            putParcelableArrayListExtra(MainService.PLAYLIST, tracks as ArrayList<Track>)
            putExtra(MainService.PLAY_POSITION, position)
        }
        return intent
    }

    private fun initializeViews() {
        fun initView() {
            mPager = findViewById(R.id.track_play_viewpager)
            mPlayerControlView = findViewById(R.id.track_player_control_view)
            mProgressBar = findViewById(R.id.progress_bar)
            mProgressText = findViewById(R.id.progress_text)
        }

        fun setupView() {
            mPager.offscreenPageLimit = 4
            mPager.adapter = mPagerAdapter
            mPager.currentItem = mInitialIntentPosition
        }

        initView()
        setupView()
    }

    private fun initObserver() {
        mViewModel.playLiveData!!.observe(this, Observer {
            Toast.makeText(this, "play status: ${it!!.code} , ${it.message}", Toast.LENGTH_SHORT).show()
        })
    }

    private fun startServiceIfNotCreated(intent: Intent) {
        if (isServiceRunning(MainService::class.java)) {

        } else {
            startMainService(intent)
        }
    }

    private fun startMainService(intent: Intent) {
        Util.startForegroundService(applicationContext, intent)
    }

    private fun isServiceRunning(serviceClass: Class<*>): Boolean {
        val activityManager: ActivityManager = getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager
        val runningServices = activityManager.getRunningServices(Integer.MAX_VALUE)
        for (service in runningServices) {
            if (serviceClass.name.equals(service.service.className)) return true
        }
        return false
    }

    inner class PlayerServiceConnection :ServiceConnection {

        override fun onServiceConnected(className: ComponentName, service: IBinder) {
            // We've bound to LocalService, cast the IBinder and get LocalService instance
            val binder = service as LocalBinder
            mService = binder.service
            mBound = true

            bindPlayerAndShowController()
            setPlayerListener()

            if (mInitialIntentPlayFrom == PLAY_FROM_APP) {
                updateCurrentPlayingTrack()
            } else {
                updateCurrentViewPager()
            }

            /**
             * wajib setelah statement controller
             */
            setViewPagerListener()
        }

        override fun onServiceDisconnected(arg0: ComponentName) {
            mBound = false
        }

        private fun bindPlayerAndShowController() {
            mPlayerControlView.player = mService.mExoPlayer
            mPlayerControlView.show()
        }

        private fun setPlayerListener() {
            mPlayerControlView.player.addListener(object : Player.DefaultEventListener() {
                override fun onTracksChanged(
                        trackGroups: TrackGroupArray?,
                        trackSelections: TrackSelectionArray?
                ) {
                    mPager.setCurrentItem(mPlayerControlView.player.currentWindowIndex, true)
                }
            })
        }

        private fun setViewPagerListener() {
            mPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
                override fun onPageScrollStateChanged(state: Int) {
                }

                override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {
                }

                override fun onPageSelected(position: Int) {
                    mPlayerControlView.player.seekTo(position, 0)
                }

            })
        }

        private fun updateCurrentPlayingTrack() {
            mService.compareAndUpdateCurrentTracks(mInitialIntentTrackList, mInitialIntentPosition)
        }

        private fun updateCurrentViewPager() {
            mPager.currentItem = mPlayerControlView.player.currentWindowIndex
        }

    }

    private fun showLoading(bool: Boolean) {
        if (bool) {
            mProgressBar.visibility = View.VISIBLE
            mProgressText.visibility = View.VISIBLE
        } else {
            mProgressBar.visibility = View.GONE
            mProgressText.visibility = View.GONE
        }
    }

    companion object {
        const val PLAYLIST = "PLAY_LIST"
        const val PLAY_POSITION = "PLAY_POSITION"

        const val PLAY_FROM = "PLAY_FROM"
        const val PLAY_FROM_APP = "PLAY_FROM_APP"
        const val PLAY_FROM_NOTIFICATION = "PLAY_FROM_NOTIFICATION"
    }
}
