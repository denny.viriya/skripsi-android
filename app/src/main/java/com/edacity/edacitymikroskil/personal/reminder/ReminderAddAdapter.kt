package com.edacity.edacitymikroskil.personal.reminder

import android.annotation.SuppressLint
import android.view.View
import com.edacity.edacitymikroskil.generic.GenericListRvAdapter
import com.edacity.edacitymikroskil.model.playlist.Playlist
import com.edacity.edacitymikroskil.personal.reminder.create.ReminderUtil
import java.text.SimpleDateFormat
import java.util.*

class ReminderAddAdapter(var dataset: List<Playlist>): GenericListRvAdapter(){

    val mUtil = ReminderUtil()
    lateinit var viewModel: ReminderViewModel
    var token = ""
    var trackId = 0

    override fun getItemCount(): Int {
        return dataset.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: GenericListViewHolder, position: Int) {
        holder.listTitle.text = dataset[position].name
        holder.listSubtitle.text = "contains: ${dataset[position].tracks.size} tracks"

        holder.container.setOnClickListener {
            val tomorrowDateTime =  mUtil.getTomorrowDateTime()
            viewModel.postReminder(token, playListId = dataset[position].id, timestamp = tomorrowDateTime)
        }

        holder.listMenu.visibility = View.GONE
    }
}