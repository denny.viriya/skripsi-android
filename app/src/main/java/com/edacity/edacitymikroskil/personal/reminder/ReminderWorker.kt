package com.edacity.edacitymikroskil.personal.reminder

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import androidx.work.Data
import androidx.work.Worker
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.model.playlist.Playlist
import com.edacity.edacitymikroskil.playlist.detail.PlaylistDetailActivity

class ReminderWorker : Worker() {

    companion object {
        internal val PLAYLIST_ID = "PLAYLIST_ID"
        internal val PLAYLIST_NAME = "PLAYLIST_NAME"
        internal val NOTIFICATION_CHANNEL_ID = "Edacity_channel_id"
        internal val NOTIFICATION_CHANNEL_NAME = "Edacity Reminder Channel"
        private val PLAYLIST_OBJECT = "PLAYLIST_OBJECT"

    }

    private lateinit var mData: Data

    override fun doWork(): Result {
        mData = inputData
        val playlistId = mData.getInt(PLAYLIST_ID, 0)
        val playlistName = mData.getString(PLAYLIST_NAME)
        val CHANNEL_ID = NOTIFICATION_CHANNEL_ID

        val intent = Intent(applicationContext, PlaylistDetailActivity::class.java).apply {
            val playlist = Playlist().apply {
                id = playlistId
                name = playlistName!!
            }
            putExtra(PLAYLIST_OBJECT, playlist)
        }
        val contentIntent = PendingIntent.getActivity(
                applicationContext,
                0,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT
        )

        val mBuilder = NotificationCompat.Builder(applicationContext, CHANNEL_ID)
                .setSmallIcon(R.drawable.music_cover_1)
                .setContentTitle("Play your reminder now")
                .setContentText("$playlistName")
                .setContentIntent(contentIntent)
                .setAutoCancel(true)
                .setCategory(NotificationCompat.CATEGORY_MESSAGE)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setStyle(NotificationCompat
                        .BigTextStyle()
                        .bigText("$playlistName"))
                .also {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        it.color = applicationContext.resources.getColor(R.color.colorPrimary, null)
                    } else {
                        it.color = applicationContext.resources.getColor(R.color.colorPrimary)
                    }
                }
                .setPriority(NotificationCompat.PRIORITY_MAX)

        val notification = mBuilder.build()

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val description = "Reminder for your playlist"
            val channel = NotificationChannel(
                    CHANNEL_ID,
                    NOTIFICATION_CHANNEL_NAME,
                    NotificationManager.IMPORTANCE_HIGH
            )
            channel.description = description
            channel.setShowBadge(true)
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = applicationContext.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
            notificationManager.notify(200, notification)
            return Result.SUCCESS
        }

        val notificationManager = NotificationManagerCompat.from(applicationContext)
        notificationManager.notify(200, notification)

        return Result.SUCCESS
    }
}