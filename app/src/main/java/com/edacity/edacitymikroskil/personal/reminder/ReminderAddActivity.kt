package com.edacity.edacitymikroskil.personal.reminder

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.widget.Toast
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.main.BaseActivity
import com.edacity.edacitymikroskil.model.playlist.PagedPlaylist
import com.edacity.edacitymikroskil.playlist.add.AddPlaylistActivity


class ReminderAddActivity: BaseActivity() {

    private val TRACK_ID = "TRACK_ID"

    private lateinit var mSessionManager: SessionManager
    private lateinit var mViewModel: ReminderViewModel
    private var mTrackId : Int = 0
    private lateinit var mReminderAdapter: ReminderAddAdapter
    private var mPlaylists: PagedPlaylist = PagedPlaylist()

    private lateinit var mTitle: AppCompatTextView
    private lateinit var mButton: AppCompatButton
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mSwipeRefreshLayout: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_search_result)
        initObject()
        initView()
        setupView()
        initListener()
        initObserver()
    }

    override fun onResume() {
        super.onResume()
        loadPlaylistData()
    }

    private fun initObject() {
        val factory = ViewModelProvider.AndroidViewModelFactory(application)
        mViewModel = ViewModelProviders.of(this, factory).get(ReminderViewModel::class.java)
        mSessionManager = SessionManager(this)
        mTrackId = intent.getIntExtra(TRACK_ID, 0)

        mReminderAdapter = ReminderAddAdapter(mPlaylists.playlists).apply {
            viewModel = mViewModel
            token = mSessionManager.accountToken.token
            trackId = mTrackId
        }
    }

    private fun initView() {
        mTitle = findViewById(R.id.layout_title)
        mButton = findViewById(R.id.select_playlist_button)
        mRecyclerView = findViewById(R.id.select_playlist_rv)
        mSwipeRefreshLayout = findViewById(R.id.swipe_refresh_layout)
    }

    private fun setupView() {
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.adapter = mReminderAdapter
    }

    private fun initObserver() {
        mViewModel.getPlaylistLiveData.observe(this, Observer {
            mPlaylists = it!!
            mReminderAdapter.dataset = it.playlists
            mReminderAdapter.notifyDataSetChanged()
        })

        mViewModel.remindersAddLiveData.observe(this, Observer {
            onBackPressed()
            Toast.makeText(this, "Successfully added playlist to reminder", Toast.LENGTH_SHORT).show()
        })

        mViewModel.remindersAddErrorLiveData.observe(this, Observer {
            Toast.makeText(this, it?.code.toString() +" "+ it?.message, Toast.LENGTH_SHORT).show()
        })
    }

    private fun initListener() {
        mButton.setOnClickListener {
            val intent = Intent(this, AddPlaylistActivity::class.java)
            startActivity(intent)
        }

        mSwipeRefreshLayout.setOnRefreshListener {
            loadPlaylistData()
        }
    }

    private fun loadPlaylistData() {
        mViewModel.getPlaylists(mSessionManager.accountToken.token)
    }
}
