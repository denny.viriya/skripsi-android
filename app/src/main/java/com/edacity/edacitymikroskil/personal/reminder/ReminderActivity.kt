package com.edacity.edacitymikroskil.personal.reminder

import android.annotation.SuppressLint
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.arch.lifecycle.ViewModelProviders
import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.os.Build
import android.os.Bundle
import android.support.design.widget.FloatingActionButton
import android.support.v4.app.NotificationCompat
import android.support.v4.app.NotificationManagerCompat
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.widget.Toast
import androidx.work.Data
import androidx.work.Worker
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.main.BaseActivity
import com.edacity.edacitymikroskil.model.room.DbWorkerThread
import com.edacity.edacitymikroskil.model.room.AppDatabase


class ReminderActivity : BaseActivity() {

    private lateinit var mViewModel: ReminderViewModel
    private lateinit var mSessionManager: SessionManager
    private lateinit var mReminderAdapter: ReminderAdapter
    private lateinit var mDbWorkerThread: DbWorkerThread
    private var mDb: AppDatabase? = null

    private lateinit var mToolbar: Toolbar
    private lateinit var mToolbarTitle: AppCompatTextView
    private lateinit var mRecyclerView: RecyclerView
    private lateinit var mSwipeRefreshlayout: SwipeRefreshLayout
    private lateinit var mFab: FloatingActionButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_set_reminder)

        initObject()
        initDbWorkerThread()
        establishDbConnection()
        injectAdapterDependency(mReminderAdapter)

        initView()
        setupView()
        initObserver()
        initListener()
    }

    override fun onResume() {
        super.onResume()
        loadReminders()
    }

    override fun onDestroy() {
        AppDatabase.sDestroyInstance()
        mDbWorkerThread.quit()
        super.onDestroy()
    }

    private fun initObject() {
        val factory = ViewModelProvider.AndroidViewModelFactory(application)
        mViewModel = ViewModelProviders.of(this, factory).get(ReminderViewModel::class.java)
        mSessionManager = SessionManager(this)
        mReminderAdapter = ReminderAdapter(this, lifecycle, mutableListOf())
    }

    private fun initDbWorkerThread() {
        mDbWorkerThread = DbWorkerThread("dbWorkerThread")
        mDbWorkerThread.start()
    }

    private fun establishDbConnection() {
        mDb = AppDatabase.sGetInstance(this)
    }

    private fun injectAdapterDependency(adapter: ReminderAdapter) {
        adapter.apply {
            viewModel = mViewModel
            token = mSessionManager.accountToken.token
            sessionManager = mSessionManager
        }
    }

    private fun initView() {
        mToolbar = findViewById(R.id.include_toolbar)
        mToolbarTitle = mToolbar.findViewById(R.id.toolbar_title)
        mRecyclerView = findViewById(R.id.reminder_recyclerview)
        mSwipeRefreshlayout = findViewById(R.id.swipe_refresh_layout)
        mFab = findViewById(R.id.fab)
        setupToolbar(mToolbar)
    }

    @SuppressLint("SetTextI18n")
    private fun setupView() {
        mToolbarTitle.text = "Reminder"
        mRecyclerView.layoutManager = LinearLayoutManager(this)
        mRecyclerView.setHasFixedSize(true)
        mRecyclerView.adapter = mReminderAdapter
    }

    private fun setupToolbar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private fun initObserver() {
        mViewModel.remindersLiveData.observe(this, Observer {
            mSwipeRefreshlayout.isRefreshing = false
            mReminderAdapter.dataset = it!!.reminders.toMutableList()
            mReminderAdapter.notifyDataSetChanged()
        })

        mViewModel.remindersErrorLiveData.observe(this, Observer {
            mSwipeRefreshlayout.isRefreshing = false
            Toast.makeText(this, "No reminder yet", Toast.LENGTH_SHORT).show()
        })

        mViewModel.reminderDeleteLiveData.observe(this, Observer {
            loadReminders()
        })

        mViewModel.reminderPatchLiveData.observe(this, Observer {
            loadReminders()
        })

    }

    private fun initListener() {
        mSwipeRefreshlayout.setOnRefreshListener {
            loadReminders()
        }

        mFab.setOnClickListener {
            startActivity(Intent(this, ReminderAddActivity::class.java))
        }

    }

    private fun loadReminders() {
        mSwipeRefreshlayout.isRefreshing = true
        mViewModel.getReminders(mSessionManager.accountToken.token)
    }

}
