package com.edacity.edacitymikroskil.personal.reminder.create

import android.support.v7.app.AppCompatActivity

import android.os.Bundle
import com.edacity.edacitymikroskil.R

class CreateNewReminderActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_reminder)
    }

    companion object {
        const val REQUEST_CODE = 141
    }
}
