package com.edacity.edacitymikroskil.personal

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.edacity.edacitymikroskil.io.RestClient
import com.edacity.edacitymikroskil.model.ResponseStatus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PersonalViewModel: ViewModel() {

    var tokenDestroyLiveData: MutableLiveData<ResponseStatus>? = null
        get() {
            if (field==null) {
                field = MutableLiveData()
            }
            return field
        }
        private set


    fun logout(token: String) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.logout().enqueue(object : Callback<ResponseStatus> {
            override fun onFailure(call: Call<ResponseStatus>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<ResponseStatus>?, response: Response<ResponseStatus>?) {
                tokenDestroyLiveData!!.value = response!!.body()
            }
        })
    }
}