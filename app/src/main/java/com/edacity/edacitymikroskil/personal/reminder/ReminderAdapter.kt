package com.edacity.edacitymikroskil.personal.reminder

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.arch.lifecycle.Lifecycle
import android.arch.lifecycle.LifecycleOwner
import android.os.Handler
import android.os.HandlerThread
import android.os.Looper
import android.os.Process
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SwitchCompat
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.model.reminder.Reminder
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.model.room.reminder.ReminderData
import com.edacity.edacitymikroskil.personal.reminder.create.ReminderUtil
import java.util.*
import android.util.Log
import android.widget.Switch
import androidx.work.*
import com.edacity.edacitymikroskil.personal.reminder.ReminderWorker.Companion.PLAYLIST_ID
import com.edacity.edacitymikroskil.personal.reminder.ReminderWorker.Companion.PLAYLIST_NAME
import com.edacity.edacitymikroskil.service.MainService
import io.reactivex.Completable
import java.util.concurrent.TimeUnit
import kotlin.collections.HashMap


class ReminderAdapter(
        val lifecycleOwner: LifecycleOwner,
        val lifecycle: Lifecycle,
        var dataset: MutableList<Reminder>
) : RecyclerView.Adapter<ReminderAdapter.ReminderViewHolder>() {

    lateinit var sessionManager: SessionManager
    lateinit var viewModel: ReminderViewModel
    lateinit var token: String
    private val mUtil = ReminderUtil()
    private val oneTimeWorkRequestBuilder = OneTimeWorkRequest.Builder(ReminderWorker::class.java)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReminderViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.holder_reminder, parent, false)

        return ReminderViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: ReminderViewHolder, position: Int) {

        val calendar = Calendar.getInstance()
        val datetime = mUtil.stringToDate(dataset[position].timestamp, mUtil.PATTERN)
        val reminderId = dataset[position].id
        val reminderPlaylistId = dataset[position].playlist.id
        val reminderPlaylistName = dataset[position].playlist.name
        var reminderDate = mUtil.extractDate(datetime)
        var reminderTime = mUtil.extractTime(datetime)
        val reminderWorkerTag = "reminder_worker_tag_$reminderId"

        holder.reminderTitle.text = reminderPlaylistName
        holder.reminderTime.text = reminderTime
        holder.reminderDate.text = reminderDate

        fun ReminderViewHolder.switchOff() {
            if (this@switchOff.reminderSwitch.isChecked) {
                this@switchOff.reminderSwitch.performClick()
            }
        }

        holder.reminderTime.setOnClickListener {
            val timePicker = TimePickerDialog(
                    holder.itemView.context,
                    R.style.AppTheme,
                    { view, hourOfDay, minute ->
                        holder.switchOff()
                        reminderTime = "${mUtil.timeToDoubleDigit(hourOfDay)}:${mUtil.timeToDoubleDigit(minute)}"
                        holder.reminderTime.text = reminderTime
                        viewModel.patchReminder(
                                sessionManager.accountToken.token,
                                reminderId,
                                constructReminder(reminderPlaylistId, reminderDate, reminderTime)
                        )
                    },
                    calendar.get(Calendar.HOUR_OF_DAY),
                    calendar.get(Calendar.MINUTE),
                    true)
            timePicker.show()
        }

        holder.reminderDate.setOnClickListener {
            val datePickerDialog = DatePickerDialog(
                    holder.itemView.context,
                    { view, year, month, dayOfMonth ->
                        holder.switchOff()
                        reminderDate = "$year-${mUtil.timeToDoubleDigit(month + 1)}-${mUtil.timeToDoubleDigit(dayOfMonth)}"
                        holder.reminderDate.text = reminderDate
                        viewModel.patchReminder(
                                sessionManager.accountToken.token,
                                reminderId,
                                constructReminder(reminderPlaylistId, reminderDate, reminderTime)
                        )
                    },
                    calendar.get(Calendar.YEAR),
                    calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DATE)
            )
            datePickerDialog.show()
        }

        holder.reminderSwitch.setOnCheckedChangeListener { buttonView, isChecked ->
            viewModel.dbInsertReminder(ReminderData(
                    id = reminderId,
                    time = reminderTime,
                    date = reminderDate,
                    active = isChecked
            ))

            if (isChecked) {
                onSwitchChecked(
                        reminderDate,
                        reminderTime,
                        reminderWorkerTag,
                        Data.Builder()
                                .putInt(PLAYLIST_ID, reminderPlaylistId)
                                .putString(PLAYLIST_NAME, reminderPlaylistName)
                                .build()
                )
            } else {
                onSwitchCheckedOff(reminderWorkerTag)
            }
        }

        holder.reminderDelete.setOnClickListener { it ->
            val alertDialogBuilder = AlertDialog.Builder(it.context)
            alertDialogBuilder
                    .setTitle(Html.fromHtml("Delete reminder <i>${dataset[position].playlist.name}</i>?"))
                    .setPositiveButton("Yes") { dialog, which ->
//                        val reminder = viewModel.dbReadReminder(reminderId)

                        onDeleteClicked(reminderId, reminderWorkerTag, ReminderData(
                                id = reminderId,
                                date = reminderDate,
                                time = reminderTime,
                                active = false
                        ))
                        removeItemView(position)

                        dialog.dismiss()
                    }
                    .setNegativeButton("No") { dialog, which ->
                        dialog.cancel()
                    }
                    .setMessage("You will lose the reminder permanently")
                    .create()
                    .show()
        }

        viewModel.allReminders.observe(lifecycleOwner, android.arch.lifecycle.Observer { reminderDataList ->
            val dbReminderIds = reminderDataList!!.map { it.id }
            if (dbReminderIds.contains(reminderId)) {
                val reminderData = reminderDataList.first { it.id == reminderId }
                holder.reminderSwitch.isChecked = reminderData.active
            }
        })
    }

    private fun onSwitchChecked(reminderDate: String, reminderTime: String, tag: String, data: Data) {
        Log.d("work manager", "checked")
        val dateList = mUtil.dateStringToListInt(reminderDate)
        val timeList = mUtil.timeStringToListInt(reminderTime)

        val currentCalendar = Calendar.getInstance()
        val reminderCalendar = createCleanCalendar().apply {
            set(Calendar.YEAR, mUtil.listToYear(dateList))
            set(Calendar.MONTH, mUtil.listToMonth(dateList))
            set(Calendar.DATE, mUtil.listToDay(dateList))
            set(Calendar.HOUR_OF_DAY, mUtil.listToHour(timeList))
            set(Calendar.MINUTE, mUtil.listToMinute(timeList))
            set(Calendar.SECOND, 0)
        }
        val timeDiff = reminderCalendar.timeInMillis - currentCalendar.timeInMillis

        if (timeDiff > 0) {
            val reminderWork = oneTimeWorkRequestBuilder.apply {
                setInitialDelay(timeDiff, TimeUnit.MILLISECONDS)
                addTag(tag)
                setInputData(data)
            }.build()

            WorkManager.getInstance().enqueue(reminderWork)
        }
        return
    }

    private fun onSwitchCheckedOff(tag: String) {
        WorkManager.getInstance().cancelAllWorkByTag(tag)
    }

    private fun onDeleteClicked(reminderId: Int, tag: String, reminderData: ReminderData) {
        viewModel.deleteReminder(sessionManager.accountToken.token, reminderId)
        viewModel.dbDeletereminder(reminderData)
        WorkManager.getInstance().cancelAllWorkByTag(tag)
    }

    private fun removeItemView(position: Int) {
        dataset.removeAt(position)
        notifyDataSetChanged()
    }

    private fun constructReminder(
            reminderPlaylistId: Int,
            reminderDate: String,
            reminderTime: String
    ): Map<String, String> {
        val map = HashMap<String, String>().apply {
            put("playlist", "$reminderPlaylistId")
            put("timestamp", "$reminderDate $reminderTime")
        }
        return map
    }

    private fun createCleanCalendar(): Calendar {
        val cal = Calendar.getInstance()
        cal.timeInMillis = System.currentTimeMillis()
        cal.clear()
        return cal
    }

    class ReminderViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val container: ConstraintLayout = itemView.findViewById(R.id.container)
        val reminderTime: AppCompatTextView = itemView.findViewById(R.id.reminder_time)
        val reminderDate: AppCompatTextView = itemView.findViewById(R.id.reminder_date)
        val reminderTitle: AppCompatTextView = itemView.findViewById(R.id.reminder_title)
        val reminderSwitch: Switch = itemView.findViewById(R.id.reminder_switch)
        val reminderDelete: AppCompatImageView = itemView.findViewById(R.id.reminder_delete)
    }
}