package com.edacity.edacitymikroskil.personal.reminder.create

import android.annotation.SuppressLint

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.time.format.DateTimeFormatter
import java.util.*

class ReminderUtil {

    val PATTERN = "yyyy-MM-dd HH:mm"

    @SuppressLint("SimpleDateFormat")
    fun stringToDate(str: String, pattern: String): Date {
        val simpleDateFormat = SimpleDateFormat(pattern)
        return simpleDateFormat.parse(str)
    }

    @Suppress("DEPRECATION")
    fun extractDate(date: Date): String {
        val fYear = date.year + 1900
        val fMonth = date.month + 1
        val fDate = date.date
        return "$fYear-${timeToDoubleDigit(fMonth)}-${timeToDoubleDigit(fDate)}"
    }

    @Suppress("DEPRECATION")
    fun extractTime(date: Date): String {
        return "${timeToDoubleDigit(date.hours)}:${timeToDoubleDigit(date.minutes)}"
    }

    fun extractDateTime(date: Date): String {
        return "${extractDate(date)} ${extractTime(date)}"
    }

    @SuppressLint("SimpleDateFormat")
    fun timeToDoubleDigit(time: Int): String {
        if (time <= 9) {
            return "0$time"
        }
        return time.toString()
    }

    fun getTomorrowDateTime() : String {
        val calendar = Calendar.getInstance()
        val date = calendar.time
        date.date += 1
        return extractDateTime(date)
    }

    fun timestampToTime(timestamp: String): String {
        val timeSplitted = timestamp.split(" ")
        return timeSplitted.last()
    }

    fun timestampToDate(timestamp: String): String {
        val timeSplitted = timestamp.split(" ")
        return timeSplitted.first()
    }

    internal fun dateStringToListInt(date: String): List<Int> {
        val cleanDate = date.trim()
        val splittedDate = cleanDate.split("-")
        val numbersDate = splittedDate.map { it.toInt() }
        return numbersDate
    }

    fun listToYear(date: List<Int>): Int {
        return date[0]
    }

    fun listToMonth(date: List<Int>): Int {
        return date[1] - 1
    }

    fun listToDay(date: List<Int>): Int {
        return date[2]
    }

    fun timeStringToListInt(time: String): List<Int> {
        val cleanTime = time.trim()
        val splittedTime = cleanTime.split(":")
        val numbersTime = splittedTime.map { it.toInt() }
        return numbersTime
    }

    fun listToHour(time: List<Int>): Int {
        return time[0]
    }

    fun listToMinute(time: List<Int>): Int {
        return time[1]
    }

}