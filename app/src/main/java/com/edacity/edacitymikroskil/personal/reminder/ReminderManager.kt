package com.edacity.edacitymikroskil.personal.reminder

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast
import androidx.work.Worker
import androidx.work.ktx.OneTimeWorkRequestBuilder
import java.util.*


class ReminderManager private constructor(
        private val alarmManager: AlarmManager,
        private val intent: Intent,
        private val pendingIntent: PendingIntent,
        private val calendar: Calendar
) {

    fun setReminder() {
        alarmManager.setExact(AlarmManager.RTC_WAKEUP, calendar.timeInMillis, pendingIntent)
    }

    class Builder(private val context: Context) {
        private val mAlarmManager = context.getSystemService(Context.ALARM_SERVICE) as AlarmManager
        private lateinit var mIntent : Intent
        private lateinit var mPendingIntent: PendingIntent
        private val mCalendar = Calendar.getInstance()

        fun setIntent(intent: Intent) : Builder{
            mIntent = intent
            mPendingIntent = PendingIntent.getBroadcast(
                    context,
                    0,
                    mIntent,
                    PendingIntent.FLAG_UPDATE_CURRENT
            )
            return this@Builder
        }

        fun setYear(year: Int) : Builder {
            mCalendar.set(Calendar.YEAR, year)
            return this@Builder
        }

        fun setMonth(month: Int): Builder {
            mCalendar.set(Calendar.MONTH, month)
            return this@Builder
        }

        fun setDay(day: Int): Builder {
            mCalendar.set(Calendar.DATE, day)
            return this@Builder
        }

        fun setHour(hour: Int): Builder {
            mCalendar.set(Calendar.HOUR_OF_DAY, hour)
            return this@Builder
        }

        fun setMinute(minute:Int): Builder {
            mCalendar.set(Calendar.MINUTE, minute)
            return this@Builder
        }

        fun setSecond(second: Int): Builder {
            mCalendar.set(Calendar.SECOND, second)
            return this@Builder
        }

        fun build(): ReminderManager {
            return ReminderManager(
                    mAlarmManager,
                    mIntent,
                    mPendingIntent,
                    mCalendar
            )
        }
    }

    class ReminderBroadcastReceiver: BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            val result = intent.getStringExtra("data")
        }
    }
}