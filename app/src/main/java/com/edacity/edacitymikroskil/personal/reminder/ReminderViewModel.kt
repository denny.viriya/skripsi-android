package com.edacity.edacitymikroskil.personal.reminder

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import com.edacity.edacitymikroskil.io.Delegate
import com.edacity.edacitymikroskil.io.RestClient
import com.edacity.edacitymikroskil.model.ResponseStatus
import com.edacity.edacitymikroskil.model.playlist.PagedPlaylist
import com.edacity.edacitymikroskil.model.reminder.PagedReminder
import com.edacity.edacitymikroskil.model.room.reminder.ReminderData
import com.edacity.edacitymikroskil.model.room.reminder.ReminderRepository
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ReminderViewModel(application: Application): AndroidViewModel(application) {

    var getPlaylistLiveData by Delegate.InitMutableLiveData<PagedPlaylist>()
    var getPlaylistErrorLiveData by Delegate.InitMutableLiveData<ResponseStatus>()
    var remindersLiveData by Delegate.InitMutableLiveData<PagedReminder>()
    var remindersErrorLiveData by Delegate.InitMutableLiveData<ResponseStatus>()
    var remindersAddLiveData by Delegate.InitMutableLiveData<ResponseStatus>()
    var remindersAddErrorLiveData by Delegate.InitMutableLiveData<ResponseStatus>()
    var reminderDeleteLiveData by Delegate.InitMutableLiveData<ResponseStatus>()
    var reminderDeleteErrorLiveData by Delegate.InitMutableLiveData<ResponseStatus>()
    var reminderPatchLiveData by Delegate.InitMutableLiveData<ResponseStatus>()
    var reminderPatchErrorLiveData by Delegate.InitMutableLiveData<ResponseStatus>()

    private var mRepository: ReminderRepository

    var allReminders: LiveData<List<ReminderData>>
        private set

    init {
        mRepository = ReminderRepository(getApplication())
        allReminders = mRepository.allReminders
    }

    fun getPlaylists(token: String) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.getPlaylists().enqueue(object : Callback<PagedPlaylist> {
            override fun onFailure(call: Call<PagedPlaylist>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<PagedPlaylist>?, response: Response<PagedPlaylist>?) {
                if (response!!.isSuccessful) {
                    getPlaylistLiveData.value = response.body()
                } else {
                    getPlaylistErrorLiveData.value = ResponseStatus(response.code(), response.message())
                }
            }
        })
    }

    fun getReminders(token: String) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.getReminders(1).enqueue(object : Callback<PagedReminder> {
            override fun onFailure(call: Call<PagedReminder>?, t: Throwable?) {
                return
            }

            override fun onResponse(call: Call<PagedReminder>, response: Response<PagedReminder>) {
                if (response.isSuccessful) {
                    remindersLiveData.value = response.body()
                } else {
                    remindersErrorLiveData.value = ResponseStatus(
                            code = response.code(),
                            message = response.message()
                    )
                }
            }
        })
    }

    fun postReminder(token: String, playListId: Int, timestamp: String) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.postReminders(playListId,timestamp).enqueue(object : Callback<ResponseStatus> {
            override fun onFailure(call: Call<ResponseStatus>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<ResponseStatus>?, response: Response<ResponseStatus>?) {
                if (response!!.isSuccessful) {
                    remindersAddLiveData.value = response.body()
                } else {
                    remindersAddErrorLiveData.value = ResponseStatus(
                            code = response.code(),
                            message = response.message()
                    )
                }
            }
        })
    }

    fun patchReminder(token: String, reminderId: Int, body: Map<String, String>) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.patchReminder(reminderId, body).enqueue(object : Callback<ResponseStatus> {
            override fun onFailure(call: Call<ResponseStatus>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<ResponseStatus>?, response: Response<ResponseStatus>?) {
                if (response!!.isSuccessful) {
                    reminderPatchLiveData.value = response.body()
                } else {
                    reminderPatchErrorLiveData.value = ResponseStatus(response.code(), response.message())
                }
            }

        })
    }

    fun deleteReminder(token: String, reminderId: Int) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.deleteReminder(reminderId).enqueue(object : Callback<ResponseStatus> {
            override fun onFailure(call: Call<ResponseStatus>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<ResponseStatus>?, response: Response<ResponseStatus>?) {
                if (response!!.isSuccessful) {
                    reminderDeleteLiveData.value = response.body()
                } else {
                    reminderDeleteErrorLiveData.value = ResponseStatus(
                            code = response.code(),
                            message = response.message()
                    )
                }
            }

        })
    }

    fun dbInsertReminder(reminderData: ReminderData) { mRepository.insertOrUpdateReminder(reminderData)}

    fun dbDeletereminder(reminderData: ReminderData) {mRepository.deleteReminder(reminderData)}

    fun dbReadReminder(reminderId: Int): LiveData<ReminderData> {  return mRepository.readReminder(reminderId) }
}