package com.edacity.edacitymikroskil.personal


import android.app.ActivityManager
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.authentication.InitialActivity
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.personal.reminder.ReminderActivity
import kotlinx.android.synthetic.main.fragment_personal.*
import android.content.DialogInterface
import android.support.v7.app.AlertDialog
import com.edacity.edacitymikroskil.main.MainActivity


/**
 * A simple [Fragment] subclass.
 * Use the [PersonalFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class PersonalFragment : Fragment() {

    private lateinit var mSessionManager: SessionManager
    private lateinit var mViewModel: PersonalViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObject()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_personal, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view = view)
        initListener()
        initObserver()
    }

    private fun initObject() {
        mSessionManager = SessionManager(this.context!!)
        mViewModel = ViewModelProviders.of(this).get(PersonalViewModel::class.java)
    }

    private fun initView(view: View) {

    }

    private fun initListener() {
        logout_button.setOnClickListener { logout() }

        personal_reminder_button.setOnClickListener {
            val intent = Intent(this.context, ReminderActivity::class.java)
            startActivity(intent)
        }

        personal_autostop.setOnClickListener {
            val data = arrayListOf<String>(
                    "10 seconds",
                    "30 minutes",
                    "1 Hour",
                    "off"
            )
            AlertDialog.Builder(this.context!!)
                    .setSingleChoiceItems(data.toTypedArray(), mSessionManager.autoStopSelectionPosition, null)
                    .setPositiveButton("OK") { dialog, whichButton ->
                        dialog.dismiss()
                        val selectedPosition = (dialog as AlertDialog).listView.checkedItemPosition
                        defineAutoStopTime(selectedPosition)
                    }
                    .show()
        }
    }

    private fun defineAutoStopTime(position: Int) {
        fun savePosition(position: Int, time: Long) {
            mSessionManager.autoStopSelectionPosition = position
            mSessionManager.autoStop = time
        }

        when (position) {
            0 -> savePosition(0, 10)
            1 -> savePosition(1, (30 * 60))
            2 -> savePosition(2, (60 * 60))
            else -> savePosition(3, 0)
        }
    }

    private fun initObserver() {
        mViewModel.tokenDestroyLiveData!!.observe(this, Observer {
            val intent = Intent(this.context, InitialActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
        })
    }

    private fun logout() {
        mViewModel.logout(mSessionManager.accountToken.token)
        mSessionManager.clearSession()

        val intent = Intent(this.context, InitialActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        activity!!.finish()
    }


    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PersonalFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() = PersonalFragment()
    }
}
