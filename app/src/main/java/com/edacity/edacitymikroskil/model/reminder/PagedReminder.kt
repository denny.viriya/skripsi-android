package com.edacity.edacitymikroskil.model.reminder

import com.edacity.edacitymikroskil.model.base.PagedData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PagedReminder: PagedData() {

    @SerializedName("results")
    @Expose
    var reminders: List<Reminder> = listOf()
}