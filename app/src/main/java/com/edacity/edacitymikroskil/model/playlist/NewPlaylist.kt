package com.edacity.edacitymikroskil.model.playlist

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class NewPlaylist() : Parcelable{

    @SerializedName("name")
    @Expose
    var name: String = ""

    @SerializedName("tracks")
    @Expose
    var tracks: List<Int> = listOf()

    constructor(parcel: Parcel) : this() {
        name = parcel.readString()
    }

    constructor(name: String) : this() {
        this.name = name
    }

    constructor(name: String, tracks: List<Int>) : this() {
        this.name = name
        this.tracks = tracks
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(name)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<NewPlaylist> {
        override fun createFromParcel(parcel: Parcel): NewPlaylist {
            return NewPlaylist(parcel)
        }

        override fun newArray(size: Int): Array<NewPlaylist?> {
            return arrayOfNulls(size)
        }
    }


}