package com.edacity.edacitymikroskil.model.auth

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AccountRegisterForm(
        var username: String = "",
        var email: String = "",
        var password: String = "",
        var retypePassword: String =""
)