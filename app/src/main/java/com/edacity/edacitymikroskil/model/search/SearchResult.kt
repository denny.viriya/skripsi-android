package com.edacity.edacitymikroskil.model.search

import android.os.Parcel
import android.os.Parcelable
import com.edacity.edacitymikroskil.model.Artist
import com.edacity.edacitymikroskil.model.Tag
import com.edacity.edacitymikroskil.model.Track
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SearchResult() : Parcelable {

    @SerializedName("tracks")
    @Expose
    var trackList: List<Track> = listOf()

    @SerializedName("artists")
    @Expose
    var artistList: List<Artist> = listOf()

    @SerializedName("tags")
    @Expose
    var tags: List<Tag> = listOf()

    constructor(parcel: Parcel) : this() {
        trackList = parcel.createTypedArrayList(Track)
        artistList = parcel.createTypedArrayList(Artist)
        tags = parcel.createTypedArrayList(Tag)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeTypedList(trackList)
        parcel.writeTypedList(artistList)
        parcel.writeTypedList(tags)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<SearchResult> {
        override fun createFromParcel(parcel: Parcel): SearchResult {
            return SearchResult(parcel)
        }

        override fun newArray(size: Int): Array<SearchResult?> {
            return arrayOfNulls(size)
        }
    }

}