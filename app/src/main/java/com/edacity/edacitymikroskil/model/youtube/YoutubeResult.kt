package com.edacity.edacitymikroskil.model.youtube

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class YoutubeResult {

    @SerializedName("kind")
    @Expose
    var kind: String? = ""

    @SerializedName("etag")
    @Expose
    var etag: String? = ""

    @SerializedName("nextPageToken")
    @Expose
    var nextPageToken: String? = ""

    @SerializedName("regionCode")
    @Expose
    var regionCode: String? = ""

    @SerializedName("items")
    @Expose
    var items: List<YoutubeItem>? =  emptyList()
}