package com.edacity.edacitymikroskil.model.paged

import android.os.Parcel
import android.os.Parcelable
import com.edacity.edacitymikroskil.model.Track
import com.edacity.edacitymikroskil.model.base.PagedData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class PagedTracks() : PagedData(), Parcelable {

    @SerializedName("results")
    @Expose
    var trackList: List<Track> = listOf()

    constructor(parcel: Parcel) : this() {
        trackList = parcel.createTypedArrayList(Track)
    }

    constructor(trackList: List<Track>) : this() {
        this.trackList = trackList
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        super.writeToParcel(parcel, flags)
        parcel.writeTypedList(trackList)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PagedTracks> {
        override fun createFromParcel(parcel: Parcel): PagedTracks {
            return PagedTracks(parcel)
        }

        override fun newArray(size: Int): Array<PagedTracks?> {
            return arrayOfNulls(size)
        }
    }


}