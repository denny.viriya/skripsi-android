package com.edacity.edacitymikroskil.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Track() : Parcelable {

    @SerializedName("id")
    @Expose
    var id: Int = 0

    @SerializedName("title")
    @Expose
    var title: String = ""

    @SerializedName("tags_frequency")
    @Expose
    var taggedList: List<Tag> = listOf()

    @SerializedName("artist")
    @Expose
    var artist: Artist = Artist(-1, "")

    @SerializedName("my_tags")
    @Expose
    var myTags: List<Tag> = listOf()

    @SerializedName("play_counts")
    @Expose
    var playCounts: Int = 0

    @SerializedName("rating_prediction")
    @Expose
    var ratingPrediction: Float = -1F

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        title = parcel.readString()
        taggedList = parcel.createTypedArrayList(Tag)
        artist = parcel.readParcelable(Artist::class.java.classLoader)
        myTags = parcel.createTypedArrayList(Tag)
        playCounts = parcel.readInt()
        ratingPrediction = parcel.readFloat()
    }

    constructor(id: Int, title: String) : this() {
        this.id = id
        this.title = title
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(title)
        parcel.writeTypedList(taggedList)
        parcel.writeParcelable(artist, flags)
        parcel.writeTypedList(myTags)
        parcel.writeInt(playCounts)
        parcel.writeFloat(ratingPrediction)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Track> {
        override fun createFromParcel(parcel: Parcel): Track {
            return Track(parcel)
        }

        override fun newArray(size: Int): Array<Track?> {
            return arrayOfNulls(size)
        }
    }

}