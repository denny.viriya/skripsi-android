package com.edacity.edacitymikroskil.model.auth

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AccountLoginForm(
        @SerializedName("username")
        @Expose
        var username: String = "",

        @SerializedName("password")
        @Expose
        var password: String = ""
)