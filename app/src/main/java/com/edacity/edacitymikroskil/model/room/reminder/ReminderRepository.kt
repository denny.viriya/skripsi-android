package com.edacity.edacitymikroskil.model.room.reminder

import android.app.Application
import android.arch.lifecycle.LiveData
import android.os.AsyncTask
import com.edacity.edacitymikroskil.model.room.AppDatabase

class ReminderRepository(application: Application) {

    private var mReminderDao: ReminderDao

    var allReminders: LiveData<List<ReminderData>>
        private set

    init {
        val database = AppDatabase.sGetInstance(application)
        mReminderDao = database!!.reminderDataDao()
        allReminders = mReminderDao.getAll()
    }

    fun insertOrUpdateReminder(reminderData: ReminderData) {
        InsertAsyncTask(mReminderDao).execute(reminderData)
    }

    fun deleteReminder(reminderData: ReminderData) {
        DeleteAsyncTask(mReminderDao).execute(reminderData)
    }

    fun readReminder(reminderId: Int) : LiveData<ReminderData>{
        val reminderLiveData = mReminderDao.read(reminderId)
        return reminderLiveData
    }

    companion object {
        private class InsertAsyncTask(dao: ReminderDao) : AsyncTask<ReminderData, Unit, Unit>() {

            private var mReminderDao: ReminderDao = dao

            override fun doInBackground(vararg params: ReminderData) {
                mReminderDao.insertOrUpdate(params[0])
            }
        }

        private class DeleteAsyncTask(dao: ReminderDao) : AsyncTask<ReminderData, Unit, Unit>() {

            private var mReminderDao: ReminderDao = dao

            override fun doInBackground(vararg params: ReminderData) {
                mReminderDao.delete(reminderData = params[0])
            }
        }
    }
}