package com.edacity.edacitymikroskil.model.auth

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class AccountAuthentication(
        @SerializedName("auth_token")
        @Expose var
        token: String,

        @SerializedName("id")
        @Expose var
        id: Int,

        @SerializedName("username")
        @Expose var
        username: String,

        @SerializedName("email")
        @Expose var
        email: String
)