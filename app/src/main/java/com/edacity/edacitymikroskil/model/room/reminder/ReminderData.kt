package com.edacity.edacitymikroskil.model.room.reminder

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "reminder_table")
data class ReminderData(
        @PrimaryKey var id: Int?,
        @ColumnInfo(name = "time") var time: String,
        @ColumnInfo(name = "date") var date: String,
        @ColumnInfo(name = "active") var active: Boolean
)