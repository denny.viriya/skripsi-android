package com.edacity.edacitymikroskil.model.youtube

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class YoutubeItem {
    @SerializedName("kind")
    @Expose
    var kind: String? = ""

    @SerializedName("etag")
    @Expose
    var etag: String? = ""

    @SerializedName("id")
    @Expose
    var id: YoutubeItemInfo? = YoutubeItemInfo()
}