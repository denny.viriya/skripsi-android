package com.edacity.edacitymikroskil.model.reminder

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

//TODO : pastikan model mana yang digunakan untuk buat reminder baru dan mana untuk get
class NewReminder {

    @SerializedName("playlist")
    @Expose
    var playlistId: Int = 0

    @SerializedName("timestamp")
    @Expose
    var timestamp: String = ""
}