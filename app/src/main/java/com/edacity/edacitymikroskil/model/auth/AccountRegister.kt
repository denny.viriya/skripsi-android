package com.edacity.edacitymikroskil.model.auth

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AccountRegister() : Parcelable {
    @SerializedName("id")
    @Expose
    var
    id: Int = 0

    @SerializedName("username")
    @Expose
    var
    username: String = ""

    @SerializedName("email")
    @Expose
    var
    email: String = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readInt()
        username = parcel.readString()
        email = parcel.readString()
    }

    constructor(id: Int, username: String, email: String) : this() {
        this.id = id
        this.username = username
        this.email = email
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(id)
        parcel.writeString(username)
        parcel.writeString(email)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AccountRegister> {
        override fun createFromParcel(parcel: Parcel): AccountRegister {
            return AccountRegister(parcel)
        }

        override fun newArray(size: Int): Array<AccountRegister?> {
            return arrayOfNulls(size)
        }
    }
}