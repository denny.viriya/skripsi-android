package com.edacity.edacitymikroskil.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class Tagged() : Parcelable {
    @SerializedName("tag")
    @Expose
    var tag: Tag = Tag()

    @SerializedName("frequency")
    @Expose
    var frequency: Int = 0

    constructor(parcel: Parcel) : this() {
        tag = parcel.readParcelable(Tag::class.java.classLoader)
        frequency = parcel.readInt()
    }

    constructor(tag: Tag, frequency: Int) : this() {
        this.tag = tag
        this.frequency = frequency
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeParcelable(tag, flags)
        parcel.writeInt(frequency)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Tagged> {
        override fun createFromParcel(parcel: Parcel): Tagged {
            return Tagged(parcel)
        }

        override fun newArray(size: Int): Array<Tagged?> {
            return arrayOfNulls(size)
        }
    }
}