package com.edacity.edacitymikroskil.model.playlist

import android.os.Parcel
import android.os.Parcelable
import com.edacity.edacitymikroskil.model.base.PagedData
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class PagedPlaylist() : PagedData() {

    @SerializedName("results")
    @Expose
    var playlists: List<Playlist> = listOf()

    constructor(parcel: Parcel) : this() {
        playlists = parcel.createTypedArrayList(Playlist)
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        super.writeToParcel(parcel, flags)
        parcel.writeTypedList(playlists)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PagedPlaylist> {
        override fun createFromParcel(parcel: Parcel): PagedPlaylist {
            return PagedPlaylist(parcel)
        }

        override fun newArray(size: Int): Array<PagedPlaylist?> {
            return arrayOfNulls(size)
        }
    }
}