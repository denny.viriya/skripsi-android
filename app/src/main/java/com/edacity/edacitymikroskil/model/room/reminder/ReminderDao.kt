package com.edacity.edacitymikroskil.model.room.reminder

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.persistence.room.*

@Dao
interface ReminderDao {

    @Query("SELECT * from reminder_table")
    fun getAll(): LiveData<List<ReminderData>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertOrUpdate(reminderData: ReminderData)

    @Query("SELECT * FROM reminder_table WHERE id = :reminderId")
    fun read(reminderId: Int): LiveData<ReminderData>

    @Query("DELETE from reminder_table")
    fun deleteAll()

    @Delete
    fun delete(reminderData: ReminderData)
}