package com.edacity.edacitymikroskil.model

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

data class ResponseStatus (
        @SerializedName("code")
        @Expose
        val code: Int = 0,

        @SerializedName("message")
        @Expose
        val message: String = ""
)