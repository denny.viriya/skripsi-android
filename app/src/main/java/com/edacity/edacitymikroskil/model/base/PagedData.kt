package com.edacity.edacitymikroskil.model.base

import android.os.Parcel
import android.os.Parcelable
import com.edacity.edacitymikroskil.model.Track
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

open class PagedData() : Parcelable {

    @SerializedName("count")
    @Expose
    var count : Int = 0

    @SerializedName("next")
    @Expose
    var next: String? = null

    @SerializedName("previous")
    @Expose
    var previous: String? = null

    constructor(parcel: Parcel) : this() {
        count = parcel.readInt()
        next = parcel.readString()
        previous = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeInt(count)
        parcel.writeString(next)
        parcel.writeString(previous)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<PagedData> {
        override fun createFromParcel(parcel: Parcel): PagedData {
            return PagedData(parcel)
        }

        override fun newArray(size: Int): Array<PagedData?> {
            return arrayOfNulls(size)
        }
    }

}