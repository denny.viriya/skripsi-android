package com.edacity.edacitymikroskil.model.youtube

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class YoutubeItemInfo {
    @SerializedName("kind")
    @Expose
    var kind: String = ""

    @SerializedName("videoId")
    @Expose
    var videoId: String = ""
}