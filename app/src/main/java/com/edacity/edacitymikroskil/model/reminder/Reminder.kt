package com.edacity.edacitymikroskil.model.reminder

import android.os.Parcel
import android.os.Parcelable
import com.edacity.edacitymikroskil.model.playlist.Playlist
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import java.sql.Timestamp

class Reminder() {

    @SerializedName("id")
    @Expose
    var id: Int = 0

    @SerializedName("playlist")
    @Expose
    var playlist: Playlist = Playlist()

    @SerializedName("timestamp")
    @Expose
    var timestamp: String = ""

    var activated: Boolean = false

}