package com.edacity.edacitymikroskil.model.auth

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class AccountLogin() : Parcelable {
    @SerializedName("token")
    @Expose
    var
    token: String = ""

    constructor(parcel: Parcel) : this() {
        token = parcel.readString()
    }

    constructor(token: String) : this() {
        this.token = token
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(token)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<AccountLogin> {
        override fun createFromParcel(parcel: Parcel): AccountLogin {
            return AccountLogin(parcel)
        }

        override fun newArray(size: Int): Array<AccountLogin?> {
            return arrayOfNulls(size)
        }
    }
}