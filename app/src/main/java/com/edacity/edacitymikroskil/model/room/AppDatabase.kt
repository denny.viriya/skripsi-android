package com.edacity.edacitymikroskil.model.room

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import com.edacity.edacitymikroskil.model.room.reminder.ReminderDao
import com.edacity.edacitymikroskil.model.room.reminder.ReminderData

@Database(entities = arrayOf(ReminderData::class), version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun reminderDataDao(): ReminderDao

    companion object {
        private var INSTANCE: AppDatabase? = null

        fun sGetInstance(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                            context.applicationContext,
                            AppDatabase::class.java,
                            "edacity_database"
                    ).build()
                }
            }
            return INSTANCE
        }

        fun sDestroyInstance() {
            INSTANCE = null
        }
    }
}