package com.edacity.edacitymikroskil.model

class GenericTrackCollection {
    var name: String = ""
    var tracks: List<Track> = listOf()
}