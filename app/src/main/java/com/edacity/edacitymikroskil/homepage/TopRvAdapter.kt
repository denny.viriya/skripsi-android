package com.edacity.edacitymikroskil.homepage

import android.content.Intent
import android.support.v7.widget.AppCompatImageView
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.CardView
import com.edacity.edacitymikroskil.model.Track
import android.support.v7.widget.RecyclerView.Adapter
import android.support.v7.widget.RecyclerView.ViewHolder
import android.view.ContextThemeWrapper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.PopupMenu
import android.widget.ProgressBar
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.model.Tag
import com.edacity.edacitymikroskil.searchpage.addsearch.AddSearchResultActivity
import com.edacity.edacitymikroskil.trackplayer.TrackPlayerActivity
import kotlinx.android.synthetic.main.holder_home_top.view.*
import java.util.ArrayList

class TopRvAdapter(var dataset: List<Track>) : Adapter<TopRvAdapter.HomeViewHolder>() {
    private val TRACK_ID = "TRACK_ID"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.holder_home_top, parent, false) as CardView

        return HomeViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun onBindViewHolder(holder: HomeViewHolder, position: Int) {
        lateinit var popupMenu: PopupMenu
        fun buildPopup() {
            val themeWrapper = ContextThemeWrapper(holder.itemView.context, R.style.ListMenu)
            popupMenu = PopupMenu(themeWrapper, holder.itemView.card_view);
            popupMenu.setOnMenuItemClickListener {
                when (it!!.itemId) {
                    R.id.add_to_playlist -> {
                        val intent = Intent(holder.itemView.context, AddSearchResultActivity::class.java)
                        intent.putExtra(TRACK_ID, dataset[position].id)
                        holder.itemView.context.startActivity(intent)
                        true
                    }
                    else -> false
                }
            }
            val inflater = popupMenu.menuInflater;
            inflater.inflate(R.menu.generic_list_menu, popupMenu.menu);
        }

        fun countTags(tags: List<Tag>): Int {
            var count = 0
            tags.forEach {
                count += it.count
            }
            return count
        }

        buildPopup()

        if (dataset[position].id == 0) {
            holder.cardProgressbar.visibility = View.VISIBLE
            holder.cardViewText.visibility = View.INVISIBLE
            holder.cardViewText2.visibility = View.INVISIBLE
        } else {
            holder.cardProgressbar.visibility = View.GONE
            holder.cardViewText.visibility = View.VISIBLE
            holder.cardViewText2.visibility = View.VISIBLE
        }

        holder.cardViewText.text = dataset[position].title
//        holder.cardViewText2.text  = "Tagged: ${countTags(dataset[position].taggedList)}"
        holder.cardViewText2.text  = "Playcounts: ${dataset[position].playCounts}"

        holder.itemView.setOnClickListener {
            val intent = Intent(holder.itemView.context, TrackPlayerActivity::class.java).apply {
                putParcelableArrayListExtra(TrackPlayerActivity.PLAYLIST, dataset as ArrayList<Track>)
                putExtra(TrackPlayerActivity.PLAY_POSITION, position)
            }
            holder.itemView.context.startActivity(intent)
        }

        holder.itemView.setOnLongClickListener {
            popupMenu.show()
            true
        }

    }

    class HomeViewHolder(itemView: View): ViewHolder(itemView) {
        val cardViewImage = itemView.findViewById<AppCompatImageView>(R.id.card_image_view)
        val cardViewText = itemView.findViewById<AppCompatTextView>(R.id.card_label)
        val cardViewText2 = itemView.findViewById<AppCompatTextView>(R.id.card_label2)
        val cardProgressbar = itemView.findViewById<ProgressBar>(R.id.card_progress_bar)
    }

}