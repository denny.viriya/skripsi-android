package com.edacity.edacitymikroskil.homepage


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.crashlytics.android.Crashlytics

import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.authentication.InitialActivity
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.model.Track
import com.edacity.edacitymikroskil.model.paged.PagedTracks
import com.edacity.edacitymikroskil.searchpage.result.TrackRvAdapter

/**
 * A simple [Fragment] subclass.
 * Use the [HomeFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class HomeFragment : Fragment() {

    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private lateinit var mViewModel: HomeViewModel
    private val dummyTracks = Array(10) { Track() }
    private var mPopularTracks: PagedTracks = PagedTracks(dummyTracks.toList())
    private var mRecommendedTracks: PagedTracks = PagedTracks()
    private lateinit var mSessionManager: SessionManager
    private lateinit var mTopRvAdapter: TopRvAdapter
    private lateinit var mRecommendedAdapter: TrackRvAdapter

    private lateinit var mTopRecyclerView: RecyclerView
    private lateinit var mRecommendLabel: AppCompatTextView
    private lateinit var mRecommendRecyclerView: RecyclerView
    private lateinit var mSwipeRefreshLayout: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObject()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_home, container, false)

        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView(view = view)
        setupView(view = view)
        initObserver()
        initListener()
    }

    override fun onResume() {
        super.onResume()
        loadPopularTracks()
        loadRecommendedTracks()
    }

    private fun initObject() {
        mViewModel = ViewModelProviders.of(this).get(HomeViewModel::class.java)
        mSessionManager = SessionManager(this.context!!)
        mTopRvAdapter = TopRvAdapter(mPopularTracks.trackList)
        mRecommendedAdapter = TrackRvAdapter(emptyList())
    }

    private fun initView(view: View) {
        mTopRecyclerView = view.findViewById(R.id.top_recycler_view)
        mRecommendLabel = view.findViewById(R.id.home_recommend_label)
        mRecommendRecyclerView = view.findViewById(R.id.recommend_recycler_view)
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout)

    }

    private fun setupView(view: View) {
        mTopRecyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.HORIZONTAL, false)
        mTopRecyclerView.setHasFixedSize(true)
        mTopRecyclerView.adapter = mTopRvAdapter

        mRecommendLabel.visibility = View.GONE

        mRecommendRecyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        mRecommendRecyclerView.setHasFixedSize(true)
        mRecommendRecyclerView.adapter = mRecommendedAdapter
    }

    private fun loadPopularTracks() {
        mViewModel.getPopularTrackList(mSessionManager.accountToken.token)
    }

    private fun loadRecommendedTracks() {
        mViewModel.getRecommended(mSessionManager.accountToken.token)
    }

    private fun initObserver() {
        mViewModel.popularLiveData!!.observe(this, Observer {
            mPopularTracks = it!!
            mTopRvAdapter.dataset = mPopularTracks.trackList
            mTopRvAdapter.notifyDataSetChanged()

            mSwipeRefreshLayout.isRefreshing = false
        })

        mViewModel.recommendedLiveData!!.observe(this, Observer {
            mRecommendedTracks = it!!
            mRecommendedAdapter = TrackRvAdapter(it.trackList)
            mRecommendRecyclerView.adapter = mRecommendedAdapter

            if (it.trackList.isNotEmpty()) mRecommendLabel.visibility = View.VISIBLE

            mSwipeRefreshLayout.isRefreshing = false
        })

        mViewModel.popularErrorLiveData!!.observe(this, Observer {
            if (it!!.code == 401) sessionExpiredLogout()
        })
    }

    private fun initListener() {
        mSwipeRefreshLayout.setOnRefreshListener {
            loadPopularTracks()
            loadRecommendedTracks()
        }
    }

    private fun sessionExpiredLogout() {
        Toast.makeText(this.context, "Your session has expired", Toast.LENGTH_SHORT).show()
        mSessionManager.clearSession()
        val intent = Intent(this.context, InitialActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
        activity!!.finish()

    }


    companion object {
        @JvmStatic
        public fun newInstance() = HomeFragment()
    }
}
