package com.edacity.edacitymikroskil.homepage

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.edacity.edacitymikroskil.io.RestClient
import com.edacity.edacitymikroskil.model.ResponseStatus
import com.edacity.edacitymikroskil.model.paged.PagedTracks
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class HomeViewModel: ViewModel() {

    var page = 1

    var popularLiveData: MutableLiveData<PagedTracks>? = null
        get() {
            if (field==null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    var popularErrorLiveData: MutableLiveData<ResponseStatus>? = null
        get() {
            if (field==null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    fun getPopularTrackList(token: String) {
        val apiInterface = RestClient.getApiInterface(token)
        val popularTrackListCallback = apiInterface.getPopularTracks(page)

        popularTrackListCallback.enqueue(object : Callback<PagedTracks>{
            override fun onFailure(call: Call<PagedTracks>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<PagedTracks>?, response: Response<PagedTracks>?) {
                if (response!!.isSuccessful) {
                    popularLiveData!!.value = response.body()
                } else {
                    popularErrorLiveData!!.value = ResponseStatus(response.code(), response.message())
                }
            }
        })
    }

    var recommendedLiveData: MutableLiveData<PagedTracks>? = null
        get() {
            if (field==null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    var recommendedErrorLiveData: MutableLiveData<ResponseStatus>? = null
        get() {
            if (field==null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    fun getRecommended(token: String) {
        val apiInterface = RestClient.getApiInterface(token)

        apiInterface.getRecommendation(page).enqueue(object : Callback<PagedTracks>{
            override fun onFailure(call: Call<PagedTracks>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<PagedTracks>?, response: Response<PagedTracks>?) {
                if (response!!.isSuccessful) {
                    recommendedLiveData!!.value = response.body()
                } else {
                    recommendedErrorLiveData!!.value = ResponseStatus(response.code(), response.message())
                }
            }
        })
    }
}