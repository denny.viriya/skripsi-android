package com.edacity.edacitymikroskil.authentication


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView

import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.main.MainActivity
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.model.auth.AccountAuthentication
import com.edacity.edacitymikroskil.model.auth.AccountRegisterForm
import com.edacity.edacitymikroskil.model.ResponseStatus
import com.edacity.edacitymikroskil.model.auth.AccountLoginForm
import com.edacity.edacitymikroskil.model.auth.AccountRegister
import com.edacity.edacitymikroskil.model.reminder.Reminder
import com.edacity.edacitymikroskil.personal.reminder.ReminderManager
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonSyntaxException
import kotlinx.android.synthetic.main.fragment_register.*

/**
 * A simple [Fragment] subclass.
 *
 */
class RegisterFragment : Fragment() {

    private lateinit var mViewModel : AuthViewModel
    private lateinit var mSessionManager: SessionManager
    private val mGson = Gson()

    private lateinit var mUsernameEditText: TextInputEditText
    private lateinit var mEmailEditText: TextInputEditText
    private lateinit var mPasswordEditText: TextInputEditText
    private lateinit var mRetypePasswordEditText: TextInputEditText
    private lateinit var mRegisterButton: Button
    private lateinit var mErrorTextView: TextView


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_register, container, false)

        initObject()
        initView(view = view)
        return view
    }

    private fun initObject() {
        mViewModel = ViewModelProviders.of(this).get(AuthViewModel::class.java)
        mSessionManager = SessionManager(this.context!!)
    }

    private fun initView(view: View) {
        with(view) {
            mUsernameEditText = findViewById(R.id.nickname_edit_text)
            mEmailEditText = findViewById(R.id.email_edit_text)
            mPasswordEditText = findViewById(R.id.password_edit_text)
            mRetypePasswordEditText = findViewById(R.id.retype_password_edit_text)
            mRegisterButton = findViewById(R.id.register_button)
            mErrorTextView = findViewById(R.id.error_textview)
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObserver()
        initListener()
    }

    private fun initObserver() {
        mViewModel.registerLiveData.observe(this, Observer {
            mSessionManager.accountIdentity = it!!
            mViewModel.handleLoginData(AccountLoginForm(
                    username = mUsernameEditText.text.toString(),
                    password = mPasswordEditText.text.toString()
            ))
        })

        mViewModel.loginLiveData.observe(this, Observer {
            mSessionManager.accountToken = it!!
            val intent = Intent(this.context, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            activity!!.finish()
        })

        mViewModel.rawRegisterErrorLiveData.observe(this, Observer { response ->
            try {
                val json = mGson.fromJson<JsonObject>(response!!.string(), JsonObject::class.java)
                for (key in json.keySet()) {
                    val errorList = mGson.fromJson(json[key], List::class.java)
                    val errorMessage = errorList.first()
                    mErrorTextView.visibility = View.VISIBLE
                    mErrorTextView.text = errorMessage.toString()
                    break
                }
            } catch (e: JsonSyntaxException) {
                Snackbar.make(
                        activity!!.findViewById(android.R.id.content),
                        "Server response incorrect, please try again later",
                        Snackbar.LENGTH_LONG
                ).show()
            }

        })
    }

    private fun onRegisterSuccess() {
        mRegisterButton.setBackgroundColor(resources.getColor(android.R.color.holo_green_light))
        mRegisterButton.text = "Register Success, please check email"
    }

    private fun initListener() {
        register_button.setOnClickListener {

            if (
                    mViewModel.blankValidator(mUsernameEditText) &&
                    mViewModel.blankValidator(mEmailEditText) &&
                    mViewModel.blankValidator(mPasswordEditText) &&
                    mViewModel.blankValidator(mRetypePasswordEditText)
            ) {
                val accountRegisterModel = AccountRegisterForm(
                        username = mUsernameEditText.text.toString(),
                        email = mEmailEditText.text.toString(),
                        password = mPasswordEditText.text.toString(),
                        retypePassword = mRetypePasswordEditText.text.toString()
                )
                mViewModel.handleRegisterData(accountRegisterModel)
            }
        }
    }


}
