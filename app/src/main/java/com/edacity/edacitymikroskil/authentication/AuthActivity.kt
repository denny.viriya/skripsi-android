package com.edacity.edacitymikroskil.authentication

import android.arch.lifecycle.ViewModelProviders
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.widget.Button
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.main.BaseActivity

class AuthActivity : BaseActivity() {
    private lateinit var mViewModel : AuthViewModel
    private lateinit var mTopLoginButton: Button
    private lateinit var mTopRegisterButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_inner_auth)
        mViewModel = ViewModelProviders.of(this).get(AuthViewModel::class.java)
        window.statusBarColor = Color.TRANSPARENT

        mTopLoginButton = findViewById(R.id.btn_top_login)
        mTopRegisterButton = findViewById(R.id.btn_top_register)

        initListener()

        val page : Boolean = intent.getBooleanExtra("fragment", true)
        when (page) {
            true -> {
                setFragment(LoginFragment())
                setButtonDrawable(mTopLoginButton, R.drawable.button_auth_left_activated)
                mTopLoginButton.setTextColor(Color.WHITE)
            }
            false -> {
                setFragment(RegisterFragment())
                setButtonDrawable(mTopRegisterButton, R.drawable.button_auth_right_activated)
                mTopRegisterButton.setTextColor(Color.WHITE)
            }
        }

    }

    private fun initListener() {
        mTopLoginButton.setOnClickListener {
            setFragment(LoginFragment())
            setButtonDrawable(mTopLoginButton, R.drawable.button_auth_left_activated)
            setButtonDrawable(mTopRegisterButton, R.drawable.button_auth_right)
            mTopLoginButton.setTextColor(Color.WHITE)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mTopRegisterButton.setTextColor(resources.getColor(R.color.colorPrimary, null))
            }else {
                mTopRegisterButton.setTextColor(resources.getColor(R.color.colorPrimary))
            }
        }


        mTopRegisterButton.setOnClickListener {
            setFragment(RegisterFragment())
            setButtonDrawable(mTopLoginButton, R.drawable.button_auth_left)
            setButtonDrawable(mTopRegisterButton, R.drawable.button_auth_right_activated)
            mTopRegisterButton.setTextColor(Color.WHITE)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                mTopLoginButton.setTextColor(resources.getColor(R.color.colorPrimary, null))
            } else {
                mTopLoginButton.setTextColor(resources.getColor(R.color.colorPrimary))
            }
        }
    }

    private fun setFragment(fragment : android.support.v4.app.Fragment) {
        val fragmentManager = supportFragmentManager
        val transaction = fragmentManager.beginTransaction()
        transaction.replace(R.id.fragment_container, fragment)
        transaction.commit()
    }

    private fun setButtonDrawable(button : Button, resId : Int) {
        button.background = resources.getDrawable(resId, null)
    }
}
