package com.edacity.edacitymikroskil.authentication


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.design.widget.TextInputEditText
import android.support.v4.app.Fragment
import android.support.v4.view.animation.FastOutLinearInInterpolator
import android.support.v4.view.animation.LinearOutSlowInInterpolator
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.main.MainActivity
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.model.auth.AccountAuthentication
import com.edacity.edacitymikroskil.model.auth.AccountLoginForm
import com.edacity.edacitymikroskil.model.ResponseStatus
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import com.edacity.edacitymikroskil.model.auth.AccountLogin
import com.google.gson.Gson
import com.google.gson.JsonObject
import com.google.gson.JsonSyntaxException
import com.transitionseverywhere.Fade
import com.transitionseverywhere.TransitionManager
import com.transitionseverywhere.TransitionSet
import com.transitionseverywhere.extra.Scale
import org.json.JSONObject


/**
 * A simple [Fragment] subclass.
 *
 */
class LoginFragment : Fragment() {
    private lateinit var mSessionManager: SessionManager
    private lateinit var mViewModel : AuthViewModel
    private val mGson = Gson()

    private lateinit var mUsernameEditText: TextInputEditText
    private lateinit var mPasswordEditText: TextInputEditText
    private lateinit var mErrorTextView: TextView
    private lateinit var mLoginButtonTransitionContainer: ViewGroup
    private lateinit var mLoginButton: Button

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view =  inflater.inflate(R.layout.fragment_login, container, false)
        initObject()
        initView(view = view)
        return view
    }

    private fun initObject() {
        mViewModel = ViewModelProviders.of(activity!!).get(AuthViewModel::class.java)
        mSessionManager = SessionManager(this.context!!)
    }

    private fun initView(view: View) {
        mUsernameEditText = view.findViewById(R.id.email_edit_text)
        mPasswordEditText = view.findViewById(R.id.password_edit_text)
        mErrorTextView = view.findViewById(R.id.error_textview)
        mLoginButtonTransitionContainer = view.findViewById(R.id.login_button_container)
        mLoginButton = mLoginButtonTransitionContainer.findViewById(R.id.login_button)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initObserver()
        initListener()
    }

    private fun initObserver() {

        mViewModel.loginLiveData.observe(this, Observer<AccountLogin> { response ->
            mSessionManager.accountToken = response!!

            val transitionSet = TransitionSet()
                    .addTransition(Scale(0.8f))
                    .addTransition(Fade())
                    .also {
                        if (mLoginButton.visibility == View.VISIBLE) {
                            it.interpolator = LinearOutSlowInInterpolator()
                        } else {
                            it.interpolator = FastOutLinearInInterpolator()
                        }
                    }

            TransitionManager.beginDelayedTransition(mLoginButtonTransitionContainer, transitionSet)
            mLoginButton.visibility = View.INVISIBLE

            val intent = Intent(this.context, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            activity!!.finish()
        })

        mViewModel.rawLoginErrorLiveData.observe(this, Observer { response ->
            try {
                val json = mGson.fromJson<JsonObject>(response!!.string(), JsonObject::class.java)
                for (key in json.keySet()) {
                    val errorList = mGson.fromJson(json[key], List::class.java)
                    val errorMessage = errorList.first()
                    mErrorTextView.visibility = View.VISIBLE
                    mErrorTextView.text = errorMessage.toString()
                    break
                }
            } catch (e: JsonSyntaxException) {
                Snackbar.make(
                        activity!!.findViewById(android.R.id.content),
                        "Server response incorrect, please try again later",
                        Snackbar.LENGTH_LONG
                ).show()
            }
        })
    }

    private fun initListener() {
        mLoginButton.setOnClickListener {
            if (
                    mViewModel.blankValidator(mUsernameEditText) &&
                    mViewModel.blankValidator(mPasswordEditText)
            ) {
                mViewModel.handleLoginData(AccountLoginForm(
                        username = mUsernameEditText.text.toString(),
                        password = mPasswordEditText.text.toString()
                ))
            }
        }
    }



}
