package com.edacity.edacitymikroskil.authentication
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.support.v7.widget.AppCompatButton
import android.view.View
import android.widget.Button
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.debug.DebugActivity
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.main.BaseActivity
import com.edacity.edacitymikroskil.main.MainActivity

class InitialActivity : BaseActivity() {
    private val FRAGMENT_KEY = "fragment"
    private lateinit var mLoginButton: Button
    private lateinit var mRegisterButton: Button
    private lateinit var mSessionManager: SessionManager
    private lateinit var mDebugButton: AppCompatButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_outer_auth)

        initObject()
        initView()
        setupView()
        initListener()
        skipLogin(checkSession())
    }

    private fun initObject() {
        mSessionManager = SessionManager(this)
    }

    private fun initView() {
        mLoginButton  = findViewById(R.id.btn_top_login)
        mRegisterButton  = findViewById(R.id.btn_top_register)
        mDebugButton = findViewById(R.id.btn_debug)
    }

    private fun setupView() {
    }

    private fun initListener() {
        mLoginButton.setOnClickListener {
            val intent = Intent(this, AuthActivity::class.java).putExtra(FRAGMENT_KEY, true)
            startActivity(intent)
        }

        mRegisterButton.setOnClickListener {
            val intent = Intent(this, AuthActivity::class.java).putExtra(FRAGMENT_KEY, false)
            startActivity(intent)
        }

        mDebugButton.setOnClickListener {
            val intent = Intent(this, DebugActivity::class.java).putExtra(FRAGMENT_KEY, false)
            startActivity(intent)
        }
    }

    private fun checkSession() : Boolean{
        if (mSessionManager.hasSession()) {
            return true
        } else {
            mSessionManager.clearSession()
            return false
        }
    }

    private fun skipLogin(isLogin: Boolean) {
        if (isLogin) {
            val intent = Intent(this, MainActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
            startActivity(intent)
            this.finish()
        }
    }

}
