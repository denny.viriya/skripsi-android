package com.edacity.edacitymikroskil.authentication

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.support.design.widget.TextInputEditText
import com.edacity.edacitymikroskil.io.Delegate
import com.edacity.edacitymikroskil.io.RestClient
import com.edacity.edacitymikroskil.model.ResponseStatus
import com.edacity.edacitymikroskil.model.auth.*
import com.google.gson.Gson
import com.google.gson.JsonObject
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.converter.scalars.ScalarsConverterFactory

class AuthViewModel : ViewModel() {
    private val mGson = Gson()

    var loginLiveData by Delegate.InitMutableLiveData<AccountLogin>()
    var registerLiveData by Delegate.InitMutableLiveData<AccountRegister>()
    var rawLoginErrorLiveData by Delegate.InitMutableLiveData<ResponseBody>()
    var rawRegisterErrorLiveData by Delegate.InitMutableLiveData<ResponseBody>()

    fun handleRegisterData(registerForm: AccountRegisterForm) {
        authRegisterVerbose(registerForm)
    }

    fun handleLoginData(requestBody: AccountLoginForm) {
        authLoginVerbose(requestBody)
    }

    private fun authLoginVerbose(requestBody: AccountLoginForm) {
        val apiInterface = RestClient.getApiInterface()
        val loginData = apiInterface.verboseLogin(requestBody)

        loginData.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if (response!!.isSuccessful) {
                    loginLiveData.value = mGson.fromJson(response.body()!!.string(), AccountLogin::class.java)
                } else {
                    rawLoginErrorLiveData.value = response.errorBody()
                }
            }
        })
    }

//    private fun authLoginVerbose(requestBody: AccountLoginForm) {
//        val apiInterface = RestClient.getApiInterface()
//        val loginData = apiInterface.verboseLogin(requestBody)
//
//        loginData.enqueue(object : Callback<ResponseBody> {
//            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {
//            }
//
//            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
//                if (response!!.isSuccessful) {
//                    loginLiveData.value = mGson.fromJson(response.body()!!.string(), AccountLogin::class.java)
//                } else {
//                    rawLoginErrorLiveData.value = response.errorBody()
//                }
//            }
//        })
//    }

    private fun authRegisterVerbose(requestBody: AccountRegisterForm) {
        val apiInterface = RestClient.getApiInterface()
        val registerData = apiInterface.verboseRegister(requestBody)

        registerData.enqueue(object : Callback<ResponseBody> {
            override fun onFailure(call: Call<ResponseBody>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<ResponseBody>?, response: Response<ResponseBody>?) {
                if (response!!.isSuccessful && response.code() == 201) { //201 created
                    registerLiveData.value = mGson.fromJson(response.body()!!.string(), AccountRegister::class.java)
                } else {
                    rawRegisterErrorLiveData.value = response.errorBody()
                }

            }
        })
    }

    internal fun blankValidator(editText: TextInputEditText) : Boolean{
        if (editText.text.isBlank()) {
            editText.error = "Field must not be blank"
            return false
        }
        return true
    }

}