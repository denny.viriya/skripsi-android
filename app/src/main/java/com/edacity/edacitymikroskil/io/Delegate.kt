package com.edacity.edacitymikroskil.io

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.util.Log
import kotlin.reflect.KProperty

class Delegate {

    class InitMutableLiveData<T> {
        var field: android.arch.lifecycle.MutableLiveData<T> = MutableLiveData()

        operator fun getValue(thisRef: Any?, p: KProperty<*>): MutableLiveData<T> {
            return field
        }

        operator fun setValue(thisRef: Any?, p: KProperty<*>, v: MutableLiveData<T>) {
            field = v
        }
    }
}