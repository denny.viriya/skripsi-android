package com.edacity.edacitymikroskil.io

import com.edacity.edacitymikroskil.model.youtube.YoutubeResult
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path

interface StreamingInterface {
    @GET("/search/{keyword}")
    fun getVideoIdFromYoutube(@Path("keyword") keyword: String): Call<YoutubeResult>
}