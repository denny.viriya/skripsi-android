package com.edacity.edacitymikroskil.io

import com.edacity.edacitymikroskil.model.ResponseStatus
import com.edacity.edacitymikroskil.model.auth.AccountLogin
import com.edacity.edacitymikroskil.model.auth.AccountLoginForm
import com.edacity.edacitymikroskil.model.auth.AccountRegister
import com.edacity.edacitymikroskil.model.auth.AccountRegisterForm
import com.edacity.edacitymikroskil.model.paged.PagedTracks
import com.edacity.edacitymikroskil.model.playlist.NewPlaylist
import com.edacity.edacitymikroskil.model.playlist.PagedPlaylist
import com.edacity.edacitymikroskil.model.playlist.Playlist
import com.edacity.edacitymikroskil.model.reminder.PagedReminder
import com.edacity.edacitymikroskil.model.search.SearchResult
import com.edacity.edacitymikroskil.model.youtube.YoutubeResult
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.*

interface ApiInterface {

    @POST("/api/login/")
    fun login(@Body accountLoginForm: AccountLoginForm): Call<AccountLogin>

    @POST("/api/login-jwt/")
    fun verboseLogin(@Body accountLoginForm: AccountLoginForm): Call<ResponseBody>

    @POST("/auth/users/create/")
    fun register(@Body accountRegisterForm: AccountRegisterForm): Call<AccountRegister>

    @POST("/auth/users/create/")
    fun verboseRegister(@Body accountRegisterForm: AccountRegisterForm): Call<ResponseBody>

    @GET("/api/popular/")
    fun getPopularTracks(@Query("page") page: Int): Call<PagedTracks>

    @GET("/api/search/{keyword}/")
    fun getSearchResult(@Path("keyword") keyword: String): Call<SearchResult>

    @POST("/api/playlists/")
    fun createPlaylist(@Body newPlaylist: NewPlaylist): Call<Playlist>

    @GET("api/playlists/")
    fun getPlaylists(): Call<PagedPlaylist>

    @GET("/api/playlists/{id}/")
    fun readPlaylist(@Path("id") id: Int): Call<Playlist>

    @DELETE("api/playlists/{id}/")
    fun deletePlaylist(@Path("id") id: Int): Call<Unit>

    @GET("/api/fresh/")
    fun getFreshTracks(@Query("page") page: Int): Call<PagedTracks>

    @FormUrlEncoded
    @POST("/api/playlist/{id}/track/")
    fun createTrackInPlaylist(@Path("id") playlistId: Int, @FieldMap body: Map<String, Int>): Call<Unit>

    @DELETE("/api/playlist/{id}/track/{pk_track}/")
    fun deleteTrackInPlaylist(@Path("id") playlistId: Int, @Path("pk_track") trackId: Int): Call<Unit>

    @GET("/api/play/{id}/")
    fun playTrack(@Path("id") trackId: Int): Call<ResponseStatus>

    @POST("/api/logout-jwt/")
    fun logout(): Call<ResponseStatus>

    @GET("/api/recommendation/")
    fun getRecommendation(@Query("page") page: Int): Call<PagedTracks>

    @GET("/api/reminders/")
    fun getReminders(@Query("page") page: Int): Call<PagedReminder>

    @FormUrlEncoded
    @POST("/api/reminders/")
    fun postReminders(@Field("playlist") playlist: Int, @Field("timestamp") timestamp: String): Call<ResponseStatus>

    @DELETE("/api/reminders/{id}/")
    fun deleteReminder(@Path("id") reminderId: Int): Call<ResponseStatus>

    @FormUrlEncoded
    @PATCH("/api/reminders/{id}/")
    fun patchReminder(@Path("id") reminderId: Int, @FieldMap body: Map<String, String>): Call<ResponseStatus>

    @GET("/api/artist/{id}/tracks/")
    fun getArtistTracks(@Path("id") artistId: Int): Call<PagedTracks>

    @GET("/api/tag/{id}/tracks/")
    fun getTagTracks(@Path("id") tagId: Int): Call<PagedTracks>
}
