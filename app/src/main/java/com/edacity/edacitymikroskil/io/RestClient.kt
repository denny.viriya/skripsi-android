package com.edacity.edacitymikroskil.io

import io.reactivex.schedulers.Schedulers
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

const val IP_LOCAL = "127.0.0.1"
const val STREAMING_IP = IP_LOCAL
const val STREAMING_PORT = "7331"

object RestClient {

//    var URL = "http://$IP:$PORT/"
    var URL = IP_LOCAL
    private var mApiInterface: ApiInterface? = null
    private var mApiToken = ""

    fun getApiInterface(token: String = ""): ApiInterface {
        if (!mApiToken.equals(token)) {
            mApiToken = token
            mApiInterface = getApiInterface(
                    URL,
                    buildOkHttpClient(mApiToken)
            )
        }
        if (mApiInterface == null) {
            mApiInterface = getApiInterface(URL, buildOkHttpClient(mApiToken))
        }
        return mApiInterface!!
    }

    fun getStreamingApiInterface(baseUrl: String = "http://$STREAMING_IP:$STREAMING_PORT/"): StreamingInterface {
        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(buildOkHttpClient(""))
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.computation()))
                .build()

        return retrofit.create(StreamingInterface::class.java)
    }


    private fun getApiInterface(baseUrl : String, okHttpClient: OkHttpClient) : ApiInterface{
        val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createWithScheduler(Schedulers.computation()))
                .build()

        return retrofit.create(ApiInterface::class.java)
    }

    private fun buildOkHttpClient(mApiToken: String) : OkHttpClient {
        val httpLoggingInterceptor = HttpLoggingInterceptor().apply {
            level = HttpLoggingInterceptor.Level.BODY
        }

        val client =  OkHttpClient
                .Builder()
                .addInterceptor(httpLoggingInterceptor)
                .connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)
                .addInterceptor { chain ->
                    val originalRequest = chain.request()
                    val request: Request

                    request = originalRequest.newBuilder()
                            .header("Accept", "application/json")
                            .also { newRequest ->
                                if (mApiToken.isNotEmpty()) {
                                    newRequest.header("Authorization", "JWT $mApiToken")
                                }
                            }
                            .method(originalRequest.method(), originalRequest.body())
                            .build()

                    val response = chain.proceed(request)
                    return@addInterceptor response
                }
                .build()

        client.dispatcher().maxRequests = 15

        return client
    }
}