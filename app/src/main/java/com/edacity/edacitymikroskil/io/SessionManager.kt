package com.edacity.edacitymikroskil.io

import android.content.Context
import android.content.SharedPreferences
import android.preference.PreferenceManager
import com.edacity.edacitymikroskil.model.auth.AccountAuthentication
import com.edacity.edacitymikroskil.model.auth.AccountLogin
import com.edacity.edacitymikroskil.model.auth.AccountRegister

class SessionManager(context: Context) {
    private val TOKEN = "account_token"
    private val ID = "account_id"
    private val USERNAME = "account_name"
    private val EMAIL = "account_email"
    private val REMINDERS = "account_reminder"
    private val AUTOSTOP = "AUTOSTOP"
    private val AUTOSTOP_POSITION = "AUTOSTOP_POSITION"

    private val mSharedPreferences: SharedPreferences
    private val mEditor: SharedPreferences.Editor

    init {
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        mEditor = mSharedPreferences.edit()
        mEditor.apply()
    }

    var account : AccountAuthentication
        get() {
            return AccountAuthentication(
                    token = mSharedPreferences.getString(TOKEN, ""),
                    id = mSharedPreferences.getInt(ID, -1),
                    username = mSharedPreferences.getString(USERNAME, ""),
                    email = mSharedPreferences.getString(EMAIL, "")
            )
        }
        set(value) {
            mEditor.putString(TOKEN, value.token)
            mEditor.putInt(ID, value.id)
            mEditor.putString(USERNAME, value.username)
            mEditor.putString(EMAIL, value.email)
            mEditor.apply()
        }

    var accountToken : AccountLogin
        get() {
            return AccountLogin(
                    token = mSharedPreferences.getString(TOKEN, "")
            )
        }
        set(value) {
            mEditor.putString(TOKEN, value.token)
            mEditor.apply()
        }

    var accountIdentity : AccountRegister
        get() {
            return AccountRegister(
                    id = mSharedPreferences.getInt(ID, -1),
                    username = mSharedPreferences.getString(USERNAME, ""),
                    email = mSharedPreferences.getString(EMAIL, "")
            )
        }
        set(value) {
            mEditor.putInt(ID, value.id)
            mEditor.putString(USERNAME, value.username)
            mEditor.putString(EMAIL, value.email)
            mEditor.apply()
        }

    var autoStop: Long
        get() {
            return mSharedPreferences.getLong(AUTOSTOP, 0)
        }
        set(value) {
            mEditor.putLong(AUTOSTOP, value)
            mEditor.apply()
        }

    var autoStopSelectionPosition: Int
        get() {
            return mSharedPreferences.getInt(AUTOSTOP_POSITION, 3)
        }
        set(value) {
            mEditor.putInt(AUTOSTOP_POSITION, value)
            mEditor.apply()
        }

    var streamingUrl: String
        get() {
            val url = mSharedPreferences.getString("STREAMING_URL", "http://$STREAMING_IP:$STREAMING_PORT/")
            return url
        }
        set(value) {
            if (value.isBlank()) {
                mEditor.putString("STREAMING_URL", "http://$STREAMING_IP:$STREAMING_PORT/")
                mEditor.apply()
            } else {
                mEditor.putString("STREAMING_URL", value)
                mEditor.apply()
            }
        }


    fun hasSession() : Boolean{
        return accountToken.token.isNotEmpty()
    }

    fun clearSession() {
        mEditor.clear()
        mEditor.apply()
    }

}
