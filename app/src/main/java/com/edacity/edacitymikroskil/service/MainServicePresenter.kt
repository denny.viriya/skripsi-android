package com.edacity.edacitymikroskil.service

import com.edacity.edacitymikroskil.io.RestClient
import com.edacity.edacitymikroskil.model.ResponseStatus
import com.edacity.edacitymikroskil.model.youtube.YoutubeResult
import io.reactivex.Single


import retrofit2.Call
import retrofit2.Callback
import retrofit2.HttpException
import retrofit2.Response

class MainServicePresenter {

    fun playTrack(token: String, trackId: Int) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.playTrack(trackId).enqueue(object : Callback<ResponseStatus> {
            override fun onResponse(call: Call<ResponseStatus>, response: Response<ResponseStatus>) {

            }

            override fun onFailure(call: Call<ResponseStatus>, t: Throwable) {

            }
        })

    }

    fun getAudioResourceLink(keyword: String, url: String = ""): Single<YoutubeResult> {
        val single = Single.create<YoutubeResult> { emitter ->
            val apiInterface = if (url.isNotBlank()) {
                RestClient.getStreamingApiInterface(url)
            } else {
                RestClient.getStreamingApiInterface()
            }
            apiInterface.getVideoIdFromYoutube(keyword).enqueue(object : Callback<YoutubeResult>{
                override fun onFailure(call: Call<YoutubeResult>?, t: Throwable?) {
                    emitter.onError(t!!)
                }

                override fun onResponse(call: Call<YoutubeResult>?, response: Response<YoutubeResult>?) {
                    if (response!!.isSuccessful) emitter.onSuccess(response.body()!!)
                    else emitter.onError(HttpException(response))
                }
            })
        }
        return single
    }


}
