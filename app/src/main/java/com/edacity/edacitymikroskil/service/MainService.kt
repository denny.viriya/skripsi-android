package com.edacity.edacitymikroskil.service

import android.app.*
import android.app.Notification
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.*
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.model.Track
import com.edacity.edacitymikroskil.trackplayer.TrackPlayerActivity
import com.google.android.exoplayer2.*
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.ui.PlayerNotificationManager
import android.hardware.*
import android.net.Uri
import android.util.Log
import android.widget.Toast
import com.edacity.edacitymikroskil.model.youtube.YoutubeResult
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.source.*
import com.google.android.exoplayer2.trackselection.TrackSelectionArray
import com.google.android.exoplayer2.upstream.*
import com.google.android.exoplayer2.util.Util
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.observers.DisposableObserver
import io.reactivex.observers.DisposableSingleObserver
import io.reactivex.schedulers.Schedulers
import java.util.*


class MainService : Service(), SensorEventListener {

    companion object {
        const val SERVICE_START = "SERVICE_START_ARGUMENTS"
        const val PLAYLIST = "PLAY_LIST"
        const val PLAY_POSITION = "PLAY_POSITION"
        const val AUDIO_NAME = "EDACITY"
        const val PLAYBACK_CHANNEL_NAME = "EDACITY_PLAYBACK"
        const val PLAYBACK_NOTIFICATION_ID = 8080
        const val NOTIFICATION_REQUEST_CODE = 911
    }

    lateinit var serviceHandler: ServiceHandler
    lateinit var serviceTracks: List<Track>
    var mExoPlayer: SimpleExoPlayer? = null
    var servicePosition: Int = 0
    val mPresenter = MainServicePresenter()

    private lateinit var mSessionManager: SessionManager
    private lateinit var mServiceLooper: Looper
    private lateinit var mPlayerNotificationManager: PlayerNotificationManager
    private lateinit var mMediaDescriptionAdapter: PlayerNotificationManager.MediaDescriptionAdapter
    private lateinit var mDataSourceFactory: DataSource.Factory
    private lateinit var mConcatenatingMediaSource: ConcatenatingMediaSource
    private lateinit var mRawResourceDataSource: RawResourceDataSource
    private val mBinder = LocalBinder()
    private var mHandlerThread: HandlerThread? = null
    private val mRawData: DataSpec = DataSpec(RawResourceDataSource.buildRawResourceUri(R.raw.mario))
    private lateinit var mSingleTrackIntent: Intent
    private lateinit var mNotification: Notification
    private lateinit var mCompositeDisposable: CompositeDisposable

    //autoStop section
    private var mAutoStopTimer: AutoStopTimer? = null
    private lateinit var mSensorManager: SensorManager
    private var mSensor: Sensor? = null
    private var mLastUpdate: Long = 0
    private var last_x: Float = 0F
    private var last_y: Float = 0F
    private var last_z: Float = 0F
    private var SHAKE_THRESHOLD: Float = 10F


    override fun onBind(intent: Intent?): IBinder? {
        return mBinder
    }

    override fun onCreate() {
        mSingleTrackIntent = Intent(this@MainService, TrackPlayerActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP or Intent.FLAG_ACTIVITY_CLEAR_TOP)
        }
        initObject()
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        retrieveIntentData(intent)
        mMediaDescriptionAdapter = createMediaDescriptionAdapter(serviceTracks)
        reloadMediaSourceAndPlayWhenReady(serviceTracks, servicePosition)
        reloadNotificationPlayer(mMediaDescriptionAdapter)
        setExoPlayerInternalListener(mExoPlayer!!)
        return START_NOT_STICKY
    }

    override fun onDestroy() {
        releaseExoPlayerResources()
        mCompositeDisposable.dispose()
        super.onDestroy()
    }

    override fun onAccuracyChanged(sensor: Sensor?, accuracy: Int) {
        // not implement
    }

    override fun onSensorChanged(event: SensorEvent) {

        fun onAccelerometerEventTriggered() {
            if (mSessionManager.autoStop != 0L) { //restart autostop timer

                mAutoStopTimer?.cancel()
                mAutoStopTimer = AutoStopTimer(mSessionManager.autoStop, 200)
                mAutoStopTimer!!.start()
            }
        }

        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            val x = event.values[0]
            val y = event.values[1]
            val z = event.values[2]

            val curTime = System.currentTimeMillis()

            if (curTime - mLastUpdate > 200) {
                val diffTime = curTime - mLastUpdate
                mLastUpdate = curTime

                val speed = Math.abs(x + y + z - last_x - last_y - last_z) / diffTime * 10000

                if (speed > SHAKE_THRESHOLD) {
                    onAccelerometerEventTriggered()
                }

                last_x = x
                last_y = y
                last_z = z
            }
        }
    }

    private fun initObject() {

        fun initPLayerAndResources() {
            mRawResourceDataSource = RawResourceDataSource(this)
            mRawResourceDataSource.open(mRawData)
            mDataSourceFactory = DataSource.Factory { mRawResourceDataSource }
            mExoPlayer = ExoPlayerFactory.newSimpleInstance(this, DefaultTrackSelector())
        }

        fun initSensor() {
            mSensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
            mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        }

        mSessionManager = SessionManager(this)
        mCompositeDisposable = CompositeDisposable()
        initPLayerAndResources()
        initSensor()

    }

    private fun createMediaDescriptionAdapter(
            tracks: List<Track>
    ): PlayerNotificationManager.MediaDescriptionAdapter {

        return object : PlayerNotificationManager.MediaDescriptionAdapter {

            override fun createCurrentContentIntent(player: Player?): PendingIntent? {

                mSingleTrackIntent.apply {
                    putParcelableArrayListExtra(TrackPlayerActivity.PLAYLIST, tracks as ArrayList<Track>)
                    putExtra(TrackPlayerActivity.PLAY_FROM, TrackPlayerActivity.PLAY_FROM_NOTIFICATION)
                }

                return PendingIntent.getActivity(
                        this@MainService,
                        NOTIFICATION_REQUEST_CODE,
                        mSingleTrackIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                )
            }

            override fun getCurrentContentText(player: Player?): String? {
                try {
                    return tracks[player!!.currentWindowIndex].artist.name
                } catch (e: IndexOutOfBoundsException) {

                }

                return "player : ${player!!.currentWindowIndex}"
            }

            override fun getCurrentContentTitle(player: Player?): String {
                try {
                    return tracks[player!!.currentWindowIndex].title
                } catch (e: IndexOutOfBoundsException) {

                }
                return "player : ${player!!.currentWindowIndex}"
            }

            override fun getCurrentLargeIcon(
                    player: Player?,
                    callback: PlayerNotificationManager.BitmapCallback?
            ): Bitmap? {
                val largeIcon = BitmapFactory.decodeResource(resources, R.drawable.music_cover_1)
                return largeIcon
            }
        }
    }

    private fun setPlayerNotificationListener(notificationManager: PlayerNotificationManager) {
        notificationManager.setNotificationListener(object : PlayerNotificationManager.NotificationListener {
            override fun onNotificationCancelled(notificationId: Int) {
                stopSelf()
            }

            override fun onNotificationStarted(notificationId: Int, notification: Notification?) {
                startForeground(notificationId, notification)
                mNotification = notification!!
            }
        })
    }

    private fun constructNotificationManager(
            adapter: PlayerNotificationManager.MediaDescriptionAdapter
    ): PlayerNotificationManager {

        return PlayerNotificationManager
                .createWithNotificationChannel(
                        this,
                        PLAYBACK_CHANNEL_NAME,
                        R.string.app_name,
                        PLAYBACK_NOTIFICATION_ID,
                        adapter
                )
    }

    private fun createConcatenateMediaSource(tracks: List<Track>): ConcatenatingMediaSource {
//        val link = "http://156.67.218.219:7331/i0p1bmr0EmE"

        val concatMediaSource = ConcatenatingMediaSource()

        for (track in tracks) {
            val mediaSource = ExtractorMediaSource
                    .Factory(mDataSourceFactory)
                    .createMediaSource(mRawResourceDataSource.uri)

            concatMediaSource.addMediaSource(mediaSource)
        }

//        for (track in tracks) {
//            val mediaSource = ExtractorMediaSource(
//                    Uri.parse(link),
//                    DefaultDataSourceFactory(this, "userAgent"),
//                    DefaultExtractorsFactory(),
//                    null,
//                    null)
//            concatMediaSource.addMediaSource(mediaSource)
//        }

        return concatMediaSource
    }

    private fun retrieveIntentData(intent: Intent?) {
        if (intent != null) {
            serviceTracks = intent.getParcelableArrayListExtra(PLAYLIST)
            servicePosition = intent.getIntExtra(PLAY_POSITION, 0)
        }
    }

    private fun releaseExoPlayerResources() {
        mPlayerNotificationManager.setPlayer(null)
        mExoPlayer!!.release()
        mExoPlayer = null
    }

    private fun setExoPlayerInternalListener(player: SimpleExoPlayer) {
        val playCountDownTimer = object : CountDownTimer(5000, 250) {
            override fun onFinish() {
                try {
                    mPresenter.playTrack(
                            mSessionManager.accountToken.token,
                            serviceTracks[mExoPlayer!!.currentWindowIndex].id
                    )
                } catch (e: KotlinNullPointerException) {
                    Toast.makeText(this@MainService, "service already shutdown", Toast.LENGTH_SHORT).show()
                } catch (e: IndexOutOfBoundsException) {

                }
            }

            override fun onTick(p0: Long) {
            }
        }

        player.addListener(object : Player.DefaultEventListener() {
            override fun onPlayerStateChanged(playWhenReady: Boolean, playbackState: Int) {
                super.onPlayerStateChanged(playWhenReady, playbackState)

                fun restartAutoStopTimer() {
                    if (mSessionManager.autoStop != 0L) { //restart autostop timer
                        Log.d("timer", "player state changed, session is ${mSessionManager.autoStop}")
                        mAutoStopTimer?.cancel()
                        mAutoStopTimer = AutoStopTimer(mSessionManager.autoStop, 200)
                        mAutoStopTimer!!.start()
                    }
                }

                fun onMusicPlaying() {
                    playCountDownTimer.start()
                    startForeground(PLAYBACK_NOTIFICATION_ID, mNotification)
                    mSensorManager.registerListener(this@MainService, mSensor, SensorManager.SENSOR_DELAY_NORMAL)
                    restartAutoStopTimer()
                }

                fun onMusicStopPlaying() {
                    playCountDownTimer.cancel()
                    stopForeground(false)
                    mSensorManager.unregisterListener(this@MainService)
                    mAutoStopTimer?.cancel()
                }

                if (mExoPlayer!!.playWhenReady) {
                    Log.d("timer", "PWR")
                    onMusicPlaying()
                } else {
                    Log.d("timer", "NOT PWR")
                    onMusicStopPlaying()
                }

            }

            override fun onPlayerError(error: ExoPlaybackException?) {
                super.onPlayerError(error)
                error?.printStackTrace()
                Toast.makeText(this@MainService, error.toString(), Toast.LENGTH_LONG).show()
            }
        })
    }

    fun compareAndUpdateCurrentTracks(tracks: List<Track>, position: Int) {

        fun areSameTracks(tracks1: List<Track>, tracks2: List<Track>): Boolean {
            val trackIds1 = tracks1.map(Track::id).sorted()
            val trackIds2 = tracks2.map(Track::id).sorted()
            return trackIds1.equals(trackIds2)
        }

        if (areSameTracks(serviceTracks, tracks)) {
            if (servicePosition.equals(position)) {
                //do nothing
            } else {
                servicePosition = position
                mExoPlayer!!.seekTo(servicePosition, 0)
            }
        } else {
            serviceTracks = tracks
            servicePosition = position

            mConcatenatingMediaSource.clear()
            reloadMediaSourceAndPlayWhenReady(serviceTracks, servicePosition)

//            mMediaDescriptionAdapter = createMediaDescriptionAdapter(serviceTracks)
//            reloadNotificationPlayer(mMediaDescriptionAdapter)
        }
    }

    private fun reloadMediaSourceAndPlayWhenReady(tracks: List<Track>, position: Int) {
//        mConcatenatingMediaSource = createConcatenateMediaSource(tracks)
//        mExoPlayer!!.prepare(mConcatenatingMediaSource)
//        mExoPlayer!!.seekTo(position, 0)
//        mExoPlayer!!.playWhenReady = true
        loadAllTrackUrls(tracks, position)
    }

    private fun loadAllTrackUrls(tracks: List<Track>, position: Int) {
        mCompositeDisposable.clear()
        val urlList = mutableListOf<String>()

        val trackList = tracks.map {
            mPresenter.getAudioResourceLink("${it.artist.name} ${it.title}", mSessionManager.streamingUrl)
        }

        mCompositeDisposable.add(Observable
                .fromIterable(trackList)
                .subscribeWith(object : DisposableObserver<Single<YoutubeResult>>() {
                    override fun onComplete() {
                    }

                    override fun onNext(t: Single<YoutubeResult>) {
                        mCompositeDisposable.add(
                                t.subscribeWith(object : DisposableSingleObserver<YoutubeResult>() {
                                    override fun onSuccess(t: YoutubeResult) { //get link
                                        val videoId = t.items?.first()?.id?.videoId ?: "DMZLvgAGx3A"

                                        urlList.add("${mSessionManager.streamingUrl}$videoId/")

                                        if (urlList.size == tracks.size) {
                                            onAllUrlReady(urlList, position)
                                        }
                                }

                                    override fun onError(e: Throwable) { //error, hardcode then
                                        urlList.add("${mSessionManager.streamingUrl}DMZLvgAGx3A/")

                                        if (urlList.size == tracks.size) {
                                            onAllUrlReady(urlList, position)
                                        }
                                    }

                                })
                        )
                    }

                    override fun onError(e: Throwable) {
                        Toast.makeText(this@MainService, e.toString(), Toast.LENGTH_SHORT).show()
                    }

                })
        )
    }

    private fun onAllUrlReady(urls: List<String>, position: Int) {
        val concatMediaSource = ConcatenatingMediaSource()
        val dataSourceFactory = DefaultHttpDataSourceFactory(Util.getUserAgent(this, "Edacity"))

        for (url in urls) {
            val mediaSource = ExtractorMediaSource
                    .Factory(dataSourceFactory)
                    .setExtractorsFactory(DefaultExtractorsFactory())
                    .createMediaSource(Uri.parse(url))
//            val mediaSource = HlsMediaSource
//                    .Factory(dataSourceFactory)
//                    .setExtractorFactory(HlsExtractorFactory.DEFAULT)
//                    .createMediaSource(Uri.parse(url))
//            val mediaSource = DashMediaSource
//                    .Factory(DefaultDashChunkSource.Factory(dataSourceFactory), dataSourceFactory)
//                    .createMediaSource(Uri.parse(url))
//            val mediaSource = ExtractorMediaSource(
//                    Uri.parse(url),
//                    DefaultDataSourceFactory(this, "userAgent"),
//                    DefaultExtractorsFactory(),
//                    null,
//                    null
//            )

            concatMediaSource.addMediaSource(mediaSource)
        }

//        mConcatenatingMediaSource = concatMediaSource
        mConcatenatingMediaSource = ConcatenatingMediaSource()
        mConcatenatingMediaSource.addMediaSource(concatMediaSource.getMediaSource(0))

        Observable
                .create<MediaSource> {emitter ->
                    for (i in 0 until concatMediaSource.size) {
                        Thread.sleep(100)
                        emitter.onNext(concatMediaSource.getMediaSource(i))
                    }
                    emitter.onComplete()
                }
                .subscribeOn(Schedulers.computation())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(object : DisposableObserver<MediaSource>(){
                    override fun onComplete() {
                        mExoPlayer!!.prepare(mConcatenatingMediaSource)
                        mExoPlayer!!.seekTo(position, 0)
                        mExoPlayer!!.playWhenReady = true

                        mMediaDescriptionAdapter = createMediaDescriptionAdapter(serviceTracks)
                        reloadNotificationPlayer(mMediaDescriptionAdapter)
                    }

                    override fun onNext(t: MediaSource) {
                        mConcatenatingMediaSource.addMediaSource(t)
                        Log.d("MEDIASOURCE", ".............OK")
                    }

                    override fun onError(e: Throwable) {
                    }
                })



    }

    private fun reloadNotificationPlayer(mediaDescriptionAdapter: PlayerNotificationManager.MediaDescriptionAdapter) {
        mPlayerNotificationManager = constructNotificationManager(mediaDescriptionAdapter)
        setPlayerNotificationListener(mPlayerNotificationManager)
        mPlayerNotificationManager.setPlayer(mExoPlayer)
    }

    private fun startHandlerThread() {
        mHandlerThread = HandlerThread(SERVICE_START, Process.THREAD_PRIORITY_BACKGROUND)
        mHandlerThread!!.start()
        mServiceLooper = mHandlerThread!!.looper
        serviceHandler = ServiceHandler(mServiceLooper)
    }

    inner class AutoStopTimer(val duration: Long, val interval: Long)
        : CountDownTimer(duration * 1000, interval) {

        init {
            Log.d("timer", "timer init")
        }

        override fun onFinish() {
            if (duration != 0L) {
                mExoPlayer?.playWhenReady = false
            }
        }

        override fun onTick(millisUntilFinished: Long) {
            Log.d("timer", "millisUntilFinished -> $millisUntilFinished")
        }

    }

    inner class LocalBinder : Binder() {
        val service: MainService
            get() = this@MainService

    }

    inner class ServiceHandler(looper: Looper) : Handler(looper) {
        override fun handleMessage(msg: Message?) {
            super.handleMessage(msg)
        }

    }

}