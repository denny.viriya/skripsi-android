package com.edacity.edacitymikroskil.debug

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatEditText
import android.support.v7.widget.AppCompatTextView
import android.widget.ImageView
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.io.STREAMING_IP
import com.edacity.edacitymikroskil.io.STREAMING_PORT
import com.edacity.edacitymikroskil.io.SessionManager

class DebugActivity : AppCompatActivity() {

    private lateinit var ipEditText: AppCompatEditText
    private lateinit var portEditText: AppCompatEditText
    private lateinit var mButton: AppCompatButton
    private lateinit var mCurrentAddress: AppCompatTextView
    private lateinit var mSessionManager: SessionManager
    private lateinit var mIpClearButton: ImageView
    private lateinit var mPortClearButton: ImageView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_debug)

        initObject()
        initView()
        setupView()

        mButton.setOnClickListener {
            if (ipEditText.text.isBlank() && portEditText.text.isBlank()) {
                onUrlSet()
            } else {
                onUrlSet("${ipEditText.text}:${portEditText.text}")
            }
        }

        mIpClearButton.setOnClickListener {
            ipEditText.text.clear()
        }

        mPortClearButton.setOnClickListener {
            portEditText.text.clear()
        }
    }

    private fun initObject() {
        mSessionManager = SessionManager(this)
    }

    private fun initView() {
        ipEditText = findViewById(R.id.ip_edit_text)
        portEditText = findViewById(R.id.port_edit_text)
        mButton = findViewById(R.id.button_save)
        mCurrentAddress = findViewById(R.id.current_ip)
        mIpClearButton = findViewById(R.id.ip_clear)
        mPortClearButton = findViewById(R.id.port_clear)
    }

    private fun setupView() {
        mCurrentAddress.text = "Current Streaming Url : ${mSessionManager.streamingUrl}"
    }

    private fun onUrlSet(ipPort: String = "") {
        if (ipPort.isNotBlank()) {
            val completeUrl = "http://$ipPort/"
            mSessionManager.streamingUrl = completeUrl
        } else {
            mSessionManager.streamingUrl = "http://$STREAMING_IP:$STREAMING_PORT/"
        }

        Snackbar.make(
                findViewById(android.R.id.content),
                mSessionManager.streamingUrl,
                Snackbar.LENGTH_SHORT
        ).show()

        mCurrentAddress.text = "Current Streaming Url : ${mSessionManager.streamingUrl}"
    }
}
