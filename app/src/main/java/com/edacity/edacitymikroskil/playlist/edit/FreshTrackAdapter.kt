package com.edacity.edacitymikroskil.playlist.edit

import android.view.ContextThemeWrapper
import android.view.MenuItem
import android.widget.PopupMenu
import android.widget.Toast
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.generic.GenericGridRvAdapter
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.model.Track

class FreshTrackAdapter(var dataset: List<Track>): GenericGridRvAdapter() {

    lateinit var viewModel : PlaylistEditViewModel
    var playlistId: Int = 0
    var token: String = ""

    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun onBindViewHolder(holder: GenericGridViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        with(holder) {
            cardTitle.text = dataset[position].title
            cardSubtitle.text = dataset[position].artist.name

            val themeWrapper = ContextThemeWrapper(holder.itemView.context, R.style.ListMenu)

            val popup = PopupMenu(themeWrapper, cardMenu).apply {
                setOnMenuItemClickListener {
                    when (it!!.itemId) {
                        R.id.add_to_playlist -> {
                            viewModel.createTrackInPlaylist(
                                    token = token,
                                    playlistId = playlistId,
                                    trackId = dataset[position].id
                            )
                            true
                        }
                        else -> false
                    }
                }
            }

            val inflater = popup.getMenuInflater();
            inflater.inflate(R.menu.generic_list_menu, popup.getMenu());

            cardMenu.setOnClickListener {
                popup.show()
            }
        }
    }

}