package com.edacity.edacitymikroskil.playlist


import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.widget.SwipeRefreshLayout
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.ScrollView

import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.model.playlist.PagedPlaylist
import com.edacity.edacitymikroskil.playlist.add.AddPlaylistActivity


/**
 * A simple [Fragment] subclass.
 * Use the [PlaylistFragment.newInstance] factory method to
 * create an instance of this fragment.
 *
 */
class PlaylistFragment : Fragment() {

    private lateinit var mViewModel: PlaylistViewModel
    private lateinit var mSessionManager: SessionManager
    private var mPlaylistAdapter: PlaylistAdapter = PlaylistAdapter(listOf())
    private var mPagedPlaylist: PagedPlaylist = PagedPlaylist()

    private lateinit var mPlaylistCreateButton: AppCompatButton
    private lateinit var mPlaylistRvContainer: ScrollView
    private lateinit var mPlaylistRecyclerView: RecyclerView
    private lateinit var mProgressBar: ProgressBar
    private lateinit var mSwipeRefreshLayout: SwipeRefreshLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initObject()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment
        val view =  inflater.inflate(R.layout.fragment_playlist, container, false)
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView(view = view)
        initListener()
        initObserver()
    }

    override fun onResume() {
        super.onResume()
        loadPlaylist()
    }

    private fun loadPlaylist() {
        activateLoading(true)
        mViewModel.getPlaylists(mSessionManager.account.token)
    }

    private fun initObject() {
        mViewModel = ViewModelProviders.of(this).get(PlaylistViewModel::class.java)
        mSessionManager = SessionManager(this.context!!)
    }

    private fun initView(view: View) {
        mPlaylistCreateButton = view.findViewById(R.id.playlist_create_button)
        mPlaylistRvContainer = view.findViewById(R.id.playlist_rv_container)
        mProgressBar = view.findViewById(R.id.progress_bar)
        mPlaylistRecyclerView = view.findViewById(R.id.playlist_recyclerview)
        mSwipeRefreshLayout = view.findViewById(R.id.swipe_refresh_layout)

        mPlaylistRecyclerView.layoutManager = LinearLayoutManager(this.context, LinearLayoutManager.VERTICAL, false)
        mPlaylistRecyclerView.setHasFixedSize(true)
        mPlaylistRecyclerView.adapter = mPlaylistAdapter
    }

    private fun initListener() {
        mPlaylistCreateButton.setOnClickListener {
            val intent = Intent(this.context, AddPlaylistActivity::class.java)
            startActivity(intent)
        }

        mSwipeRefreshLayout.setOnRefreshListener {
            loadPlaylist()
        }
    }

    private fun initObserver() {
        mViewModel.getPlaylistLiveData!!.observe(this, Observer {
            mPagedPlaylist = it!!
            mPlaylistAdapter.playlists = mPagedPlaylist.playlists
            mPlaylistAdapter.notifyDataSetChanged()
            activateLoading(false)
        })

        mViewModel.getPlaylistErrorLiveData!!.observe(this, Observer {
            activateLoading(false)
        })
    }

    private fun activateLoading(bool: Boolean) {
        if (bool) {
            mPlaylistCreateButton.visibility = View.INVISIBLE
            mPlaylistRecyclerView.visibility = View.INVISIBLE
            mProgressBar.visibility = View.VISIBLE
        } else {
            mPlaylistCreateButton.visibility = View.VISIBLE
            mPlaylistRecyclerView.visibility = View.VISIBLE
            mProgressBar.visibility = View.GONE
        }
    }

    companion object {
        /**
         * Use this factory method to create a new instance of
         * this fragment using the provided parameters.
         *
         * @param param1 Parameter 1.
         * @param param2 Parameter 2.
         * @return A new instance of fragment PlaylistFragment.
         */
        // TODO: Rename and change types and number of parameters
        @JvmStatic
        fun newInstance() = PlaylistFragment()
    }
}
