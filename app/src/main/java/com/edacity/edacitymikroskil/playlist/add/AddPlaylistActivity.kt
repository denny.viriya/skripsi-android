package com.edacity.edacitymikroskil.playlist.add

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatEditText
import android.widget.Toast
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.main.BaseActivity
import com.edacity.edacitymikroskil.model.playlist.NewPlaylist

class AddPlaylistActivity : BaseActivity() {

    private val PLAYLIST_NAME = "PLAYLIST_NAME"

    private lateinit var mViewModel: AddPlaylistViewModel
    private lateinit var mSessionManager: SessionManager

    private lateinit var mAddPlaylistName: AppCompatEditText
    private lateinit var mAddButton: AppCompatButton

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_playlist)

        initObject()
        initView()
        initListener()
        initObserver()
    }

    private fun initObject() {
        mViewModel = ViewModelProviders.of(this).get(AddPlaylistViewModel::class.java)
        mSessionManager = SessionManager(this)
    }

    private fun initView() {
        mAddPlaylistName = findViewById(R.id.add_playlist_name)
        mAddButton = findViewById(R.id.add_playlist_button)
    }

    private fun initListener() {
        mAddButton.setOnClickListener {
            when {
                mAddPlaylistName.text.isNullOrBlank() -> Toast.makeText(this, "Playlist name must not be Empty!", Toast.LENGTH_SHORT).show()
                else -> {
                    val newPlaylist = NewPlaylist(mAddPlaylistName.text.toString())
                    mViewModel.createPlaylist(mSessionManager.account.token, newPlaylist)
                }
            }

        }
    }

    private fun initObserver() {
        mViewModel.createdPlaylistLiveData!!.observe(this, Observer {
            finish()
        })

        mViewModel.createdPlaylistErrorLiveData!!.observe(this, Observer {
            Snackbar.make(findViewById(android.R.id.content), "Playlist with same name already exist", Snackbar.LENGTH_SHORT).show()
        })
    }
}
