package com.edacity.edacitymikroskil.playlist

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.edacity.edacitymikroskil.io.Delegate
import com.edacity.edacitymikroskil.io.RestClient
import com.edacity.edacitymikroskil.model.ResponseStatus
import com.edacity.edacitymikroskil.model.playlist.PagedPlaylist
import com.edacity.edacitymikroskil.model.playlist.Playlist
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

open class PlaylistViewModel: ViewModel() {

    var getPlaylistLiveData by Delegate.InitMutableLiveData<PagedPlaylist>()
    var getPlaylistErrorLiveData by Delegate.InitMutableLiveData<ResponseStatus>()

    fun getPlaylists(token: String) {

        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.getPlaylists().enqueue(object : Callback<PagedPlaylist> {
            override fun onFailure(call: Call<PagedPlaylist>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<PagedPlaylist>?, response: Response<PagedPlaylist>?) {
                if (response!!.isSuccessful) {
                    getPlaylistLiveData.value = response.body()
                } else {
                    getPlaylistErrorLiveData.value = ResponseStatus(response.code(), response.message())
                }
            }
        })
    }

}