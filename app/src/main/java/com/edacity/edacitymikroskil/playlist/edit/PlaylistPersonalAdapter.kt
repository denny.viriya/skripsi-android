package com.edacity.edacitymikroskil.playlist.edit

import android.content.Intent
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.AppCompatImageButton
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.model.Track
import com.edacity.edacitymikroskil.trackplayer.TrackPlayerActivity
import java.util.ArrayList

class PlaylistPersonalAdapter(var dataset: MutableList<Track>) : RecyclerView.Adapter<PlaylistPersonalAdapter.PlaylistEditViewHolder>(){

    lateinit var viewModel: PlaylistEditViewModel
    var playlistId: Int = 0
    var token: String = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PlaylistEditViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = inflater.inflate(R.layout.holder_track, parent, false)

        return PlaylistEditViewHolder(view)
    }

    override fun getItemCount(): Int {
        return dataset.size
    }

    override fun onBindViewHolder(holder: PlaylistEditViewHolder, position: Int) {
        holder.title.text = dataset[position].title
        holder.subtitle.text = dataset[position].artist.name
        holder.option.setOnClickListener {
            viewModel.deleteTrackInPlaylist(token, playlistId, dataset[position].id)
        }

        holder.container.setOnClickListener {
            val intent = Intent(holder.itemView.context, TrackPlayerActivity::class.java).apply {
                putParcelableArrayListExtra(TrackPlayerActivity.PLAYLIST, dataset as ArrayList<Track>)
                putExtra(TrackPlayerActivity.PLAY_POSITION, position)
            }
            holder.itemView.context.startActivity(intent)
        }
    }

    class PlaylistEditViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title = itemView.findViewById<AppCompatTextView>(R.id.track_title)
        val subtitle = itemView.findViewById<AppCompatTextView>(R.id.track_subtitle)
        val option = itemView.findViewById<AppCompatImageButton>(R.id.track_option_button)
        val container =  itemView.findViewById<ConstraintLayout>(R.id.container)
    }
}