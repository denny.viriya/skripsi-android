package com.edacity.edacitymikroskil.playlist

import android.annotation.SuppressLint
import android.content.Intent
import android.support.v4.content.ContextCompat.startActivity
import android.view.View
import com.edacity.edacitymikroskil.generic.GenericListRvAdapter
import com.edacity.edacitymikroskil.model.playlist.Playlist
import com.edacity.edacitymikroskil.playlist.detail.PlaylistDetailActivity

class PlaylistAdapter(var playlists: List<Playlist>) : GenericListRvAdapter() {

    private val PLAYLIST_OBJECT = "PLAYLIST_OBJECT"

    override fun getItemCount(): Int {
        return playlists.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: GenericListViewHolder, position: Int) {
        holder.listTitle.text = playlists[position].name
        holder.listSubtitle.text = "contains: ${playlists[position].tracks.size} tracks"

        holder.listMenu.visibility = View.INVISIBLE
        holder.container.setOnClickListener {
            val intent = Intent(holder.itemView.context, PlaylistDetailActivity::class.java).apply {
                putExtra(PLAYLIST_OBJECT, playlists[position])
            }

            startActivity(holder.itemView.context, intent, null)
        }
    }

}