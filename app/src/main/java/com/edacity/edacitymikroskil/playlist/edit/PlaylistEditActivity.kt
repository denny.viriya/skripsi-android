package com.edacity.edacitymikroskil.playlist.edit

import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ProgressBar
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.main.BaseActivity
import com.edacity.edacitymikroskil.model.Track
import com.edacity.edacitymikroskil.model.paged.PagedTracks
import com.edacity.edacitymikroskil.model.playlist.Playlist

class PlaylistEditActivity : BaseActivity() {

    private val PLAYLIST_OBJECT = "PLAYLIST_OBJECT"

    private lateinit var mSessionManager: SessionManager
    private lateinit var mViewModel: PlaylistEditViewModel
    private lateinit var mPersonalPlaylist: Playlist
    private lateinit var mPersonalTrackAdapter: PlaylistPersonalAdapter
    private lateinit var mFreshTracks: PagedTracks
    private lateinit var mFreshTrackAdapter: FreshTrackAdapter

    private lateinit var mPlaylistTitle: AppCompatTextView
    private lateinit var mPlaylistSubtitle: AppCompatTextView
    private lateinit var mPlaylistRecyclerView: RecyclerView
    private lateinit var mFreshTrackRecyclerView: RecyclerView
    private lateinit var mPersonalTrackProgressbar: ProgressBar

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_playlist_edit)

        initObject()
        initView()
        setupView()
        initObserver()
    }

    override fun onResume() {
        super.onResume()
        loadPersonalPlaylist(mPersonalPlaylist.id)
        loadFreshTrack()
    }

    private fun initObject() {
        mSessionManager = SessionManager(this)
        mViewModel = ViewModelProviders.of(this).get(PlaylistEditViewModel::class.java)

        mPersonalPlaylist = intent.getParcelableExtra(PLAYLIST_OBJECT)
        mPersonalTrackAdapter = PlaylistPersonalAdapter(mutableListOf())
                .apply {
                    viewModel = mViewModel
                    playlistId = mPersonalPlaylist.id
                    token = mSessionManager.accountToken.token
                }

        mFreshTracks = PagedTracks()
        mFreshTrackAdapter = FreshTrackAdapter(mFreshTracks.trackList)
                .apply {
                    viewModel = mViewModel
                    playlistId = mPersonalPlaylist.id
                    token = mSessionManager.accountToken.token
                }
    }

    private fun initView() {
        mPlaylistTitle = findViewById(R.id.edit_playlist_title)
        mPlaylistSubtitle = findViewById(R.id.edit_playlist_subtitle)
        mPlaylistRecyclerView = findViewById(R.id.user_playlist_rv)
        mFreshTrackRecyclerView = findViewById(R.id.fresh_track_rv)
        mPersonalTrackProgressbar = findViewById(R.id.personal_track_progress_bar)
    }

    @SuppressLint("SetTextI18n")
    private fun setupView() {
        mPlaylistTitle.text = mPersonalPlaylist.name
        mPlaylistSubtitle.text = "contain : ${mPersonalPlaylist.tracks.size} musics"

        mPlaylistRecyclerView.layoutManager = LinearLayoutManager(this)
        mPlaylistRecyclerView.setHasFixedSize(true)
        mPlaylistRecyclerView.adapter = mPersonalTrackAdapter

        mFreshTrackRecyclerView.layoutManager = GridLayoutManager(
                this,
                2,
                LinearLayoutManager.VERTICAL,
                false
        )
        mFreshTrackRecyclerView.setHasFixedSize(true)
        mFreshTrackRecyclerView.adapter = mFreshTrackAdapter
    }

    private fun loadFreshTrack() {
        mViewModel.getFreshTracks(mSessionManager.accountToken.token)
    }

    private fun initObserver() {
        mViewModel.personalPlaylistLiveData!!.observe(this, Observer { playlist ->
            mPersonalTrackAdapter.dataset = playlist!!.tracks.toMutableList()
            mPersonalTrackAdapter.dataset.sortBy { track: Track -> track.title }
            mPersonalTrackAdapter.notifyDataSetChanged()
            mPlaylistSubtitle.text = "contains: ${playlist.tracks.size} musics"

            showPersonalPlaylistsProgressBar(false)
        })

        mViewModel.freshTracksLiveData!!.observe(this, Observer {
            mFreshTrackAdapter.dataset = it!!.trackList
            mFreshTrackAdapter.notifyDataSetChanged()
        })

        mViewModel.createTrackLiveData!!.observe(this, Observer {playlistId ->
            loadPersonalPlaylist(playlistId!!)
        })


        mViewModel.deleteTrackLivedata!!.observe(this, Observer {
            loadPersonalPlaylist(it!!)
        })
    }

    private fun loadPersonalPlaylist(playlistId: Int) {
        showPersonalPlaylistsProgressBar(true)
        mViewModel.readPlaylist(
                token = mSessionManager.accountToken.token,
                playlistId = playlistId
        )
    }

    private fun showPersonalPlaylistsProgressBar(value: Boolean) {
        if (value) {
            mPersonalTrackProgressbar.visibility = View.VISIBLE
        } else {
            mPersonalTrackProgressbar.visibility = View.GONE
        }
    }
}
