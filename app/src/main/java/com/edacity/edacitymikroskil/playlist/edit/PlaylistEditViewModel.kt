package com.edacity.edacitymikroskil.playlist.edit


import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.util.Log
import com.edacity.edacitymikroskil.io.RestClient
import com.edacity.edacitymikroskil.model.ResponseStatus
import com.edacity.edacitymikroskil.model.paged.PagedTracks
import com.edacity.edacitymikroskil.model.playlist.Playlist
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PlaylistEditViewModel: ViewModel() {

    var freshTracksLiveData: MutableLiveData<PagedTracks>? = null
        get() {
            if (field == null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    var freshTracksErrorLiveData: MutableLiveData<ResponseStatus>? = null
        get() {
            if (field == null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    var createTrackLiveData: MutableLiveData<Int>? = null
        get() {
            if (field == null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    var createTrackErrorLiveData: MutableLiveData<Int>? = null
        get() {
            if (field == null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    var personalPlaylistLiveData: MutableLiveData<Playlist>? = null
        get() {
            if (field == null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    var personalPlaylistErrorLiveData: MutableLiveData<ResponseStatus>? = null
        get() {
            if (field == null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    var deleteTrackLivedata: MutableLiveData<Int>? = null
        get() {
            if (field == null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    var deleteTrackErrorLivedata: MutableLiveData<Int>? = null
        get() {
            if (field == null) {
                field = MutableLiveData()
            }
            return field
        }
        private set


    fun getFreshTracks(token: String) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.getFreshTracks(1).enqueue(object : Callback<PagedTracks> {
            override fun onFailure(call: Call<PagedTracks>?, t: Throwable?) {
                Log.d("ERROR", t!!.message)
            }

            override fun onResponse(call: Call<PagedTracks>?, response: Response<PagedTracks>?) {
                if (response!!.isSuccessful) {
                    freshTracksLiveData!!.value = response.body()
                } else {
                    freshTracksErrorLiveData!!.value = ResponseStatus(response.code(), response.message())
                }
            }
        })
    }

    fun createTrackInPlaylist(token: String, playlistId: Int, trackId: Int) {
        val apiInterface = RestClient.getApiInterface(token)
        val body = composeHashMap("track_id", trackId)

        apiInterface.createTrackInPlaylist(playlistId, body).enqueue(object : Callback<Unit> {
            override fun onFailure(call: Call<Unit>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<Unit>?, response: Response<Unit>?) {
                if (response!!.isSuccessful) {
                    createTrackLiveData!!.value = playlistId
                } else {
                    createTrackErrorLiveData!!.value = playlistId
                }
            }

        })
    }

    fun deleteTrackInPlaylist(token: String, playlistId: Int, trackId: Int) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.deleteTrackInPlaylist(playlistId, trackId).enqueue(object : Callback<Unit> {
            override fun onFailure(call: Call<Unit>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<Unit>?, response: Response<Unit>?) {
                if (response!!.isSuccessful) {
                    deleteTrackLivedata!!.value = playlistId
                } else {
                    deleteTrackErrorLivedata!!.value = playlistId
                }
            }
        })
    }

    fun readPlaylist(token: String, playlistId: Int) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.readPlaylist(playlistId).enqueue(object : Callback<Playlist> {
            override fun onFailure(call: Call<Playlist>?, t: Throwable?) {

            }

            override fun onResponse(call: Call<Playlist>?, response: Response<Playlist>?) {
                if (response!!.isSuccessful) {
                    personalPlaylistLiveData!!.value = response.body()
                } else {
                    personalPlaylistErrorLiveData!!.value = ResponseStatus(response.code(), response.message())
                }
            }
        })
    }

    private fun composeHashMap(key: String, value: Int): Map<String, Int> {
        val body = HashMap<String, Int>()
        body[key] = value
        return body
    }
}