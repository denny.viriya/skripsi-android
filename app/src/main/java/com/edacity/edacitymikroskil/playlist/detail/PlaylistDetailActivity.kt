package com.edacity.edacitymikroskil.playlist.detail

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.AppCompatButton
import android.support.v7.widget.AppCompatTextView
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.io.SessionManager
import com.edacity.edacitymikroskil.main.BaseActivity
import com.edacity.edacitymikroskil.model.Track
import com.edacity.edacitymikroskil.model.playlist.Playlist
import com.edacity.edacitymikroskil.playlist.edit.PlaylistEditActivity
import android.text.Html
import android.view.View
import android.widget.Toast
import com.edacity.edacitymikroskil.trackplayer.TrackPlayerActivity
import java.util.ArrayList


class PlaylistDetailActivity : BaseActivity() {

    companion object {
        const val PLAYLIST_OBJECT = "PLAYLIST_OBJECT"
    }

    private lateinit var mPlaylist: Playlist
    private lateinit var mViewModel: PlaylistDetailViewModel
    private lateinit var mSessionManager: SessionManager

    private lateinit var mDetailTitle: AppCompatTextView
    private lateinit var mDetailSubtitle: AppCompatTextView
    private lateinit var mDetailEditButton: AppCompatButton
    private lateinit var mDetailDeleteButton: AppCompatButton
    private lateinit var mDetailPlayButton: AppCompatButton
    private lateinit var mDetailTracksListTextView: AppCompatTextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_playlist_detail)

        initObject()
        initView()
        setupView(mPlaylist)
        initListener()
        initObserver()
    }

    override fun onResume() {
        super.onResume()
        loadPlaylist()
    }

    private fun initObject() {
        mViewModel = ViewModelProviders.of(this).get(PlaylistDetailViewModel::class.java)
        mSessionManager = SessionManager(this)
        mPlaylist = intent.getParcelableExtra(PLAYLIST_OBJECT)
    }

    private fun initView() {
        mDetailTitle = findViewById(R.id.detail_title)
        mDetailSubtitle = findViewById(R.id.detail_subtitle)
        mDetailEditButton = findViewById(R.id.detail_edit_button)
        mDetailDeleteButton = findViewById(R.id.detail_delete_button)
        mDetailTracksListTextView = findViewById(R.id.detail_tracks_list_tv)
        mDetailPlayButton = findViewById(R.id.detail_play_button)
    }

    private fun setupView(playlist: Playlist) {
        mDetailTitle.text = playlist.name
        mDetailSubtitle.text = "contains : ${playlist.tracks.size} tracks"
        mDetailTracksListTextView.text = Html.fromHtml(constructStrings(playlist.tracks))
    }

    private fun constructStrings(tracks: List<Track>) : String {

        var result = ""
        tracks.forEachIndexed { index, track ->
            when {
                (index < 9) -> {
                    result += "<b>${track.title}</b> <i>${track.artist.name}</i>"
                    if (index < tracks.size - 1) {
                        result += " | "
                    }
                }
            }
        }
        return result
    }

    private fun initListener() {
        val editListener = View.OnClickListener {
            val intent = Intent(this, PlaylistEditActivity::class.java)
            intent.putExtra(PLAYLIST_OBJECT, mPlaylist)
            startActivity(intent)
        }

        mDetailEditButton.setOnClickListener(editListener)

        mDetailTracksListTextView.setOnClickListener(editListener)

        mDetailDeleteButton.setOnClickListener {
            mViewModel.deletePlaylist(mSessionManager.account.token, mPlaylist.id)
        }

        mDetailPlayButton.setOnClickListener {
            if (mPlaylist.tracks.isEmpty()) {
                Toast.makeText(this, "You have no Tracks in this playlist!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            val intent = Intent(this, TrackPlayerActivity::class.java).apply {
                putParcelableArrayListExtra(TrackPlayerActivity.PLAYLIST, mPlaylist.tracks as ArrayList<Track>)
                putExtra(TrackPlayerActivity.PLAY_POSITION, 0)
            }
            this.startActivity(intent)
        }
    }

    private fun initObserver() {
        mViewModel.playlistDeleteLiveData.observe(this, Observer {
            onBackPressed()
        })

        mViewModel.playlistLiveData.observe(this, Observer {
            mPlaylist = it!!
            setupView(mPlaylist)
//            mDetailTitle.text = it.name
//            mDetailSubtitle.text = "contains: ${it.tracks.size} tracks"
//            mDetailTracksListTextView.text = Html.fromHtml(constructStrings(it.tracks))
        })
    }

    private fun loadPlaylist() {
        mViewModel.readPlaylist(mSessionManager.accountToken.token, mPlaylist.id)
    }

}
