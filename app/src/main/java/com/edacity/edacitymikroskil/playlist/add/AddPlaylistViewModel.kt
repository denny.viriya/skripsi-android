package com.edacity.edacitymikroskil.playlist.add

import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import com.edacity.edacitymikroskil.io.RestClient
import com.edacity.edacitymikroskil.model.ResponseStatus
import com.edacity.edacitymikroskil.model.playlist.NewPlaylist
import com.edacity.edacitymikroskil.model.playlist.Playlist
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddPlaylistViewModel: ViewModel() {

    var createdPlaylistLiveData: MutableLiveData<Playlist>? = null
        get() {
            if (field == null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    var createdPlaylistErrorLiveData: MutableLiveData<ResponseStatus>? = null
        get() {
            if (field == null) {
                field = MutableLiveData()
            }
            return field
        }
        private set

    fun createPlaylist(token: String, newPlaylist: NewPlaylist) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.createPlaylist(newPlaylist).enqueue(object : Callback<Playlist> {
            override fun onFailure(call: Call<Playlist>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<Playlist>, response: Response<Playlist>) {
                if (response.isSuccessful) {
                    createdPlaylistLiveData!!.value = response.body()
                } else {
                    createdPlaylistErrorLiveData!!.value  = ResponseStatus(response.code(), response.message())
                }
            }
        })
    }

}