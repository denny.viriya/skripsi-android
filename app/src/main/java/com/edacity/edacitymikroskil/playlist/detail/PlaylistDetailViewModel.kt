package com.edacity.edacitymikroskil.playlist.detail

import android.arch.lifecycle.ViewModel
import com.edacity.edacitymikroskil.io.Delegate
import com.edacity.edacitymikroskil.io.RestClient
import com.edacity.edacitymikroskil.model.ResponseStatus
import com.edacity.edacitymikroskil.model.playlist.PagedPlaylist
import com.edacity.edacitymikroskil.model.playlist.Playlist
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class PlaylistDetailViewModel: ViewModel() {

    var playlistLiveData by Delegate.InitMutableLiveData<Playlist>()
    var playlistErrorLiveData by Delegate.InitMutableLiveData<ResponseStatus>()
    var playlistDeleteLiveData by Delegate.InitMutableLiveData<ResponseStatus>()

    fun readPlaylist(token: String, playlistId: Int) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.readPlaylist(playlistId).enqueue(object : Callback<Playlist> {
            override fun onFailure(call: Call<Playlist>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<Playlist>?, response: Response<Playlist>?) {
                if (response!!.isSuccessful) {
                    playlistLiveData.value = response.body()
                } else {
                    playlistErrorLiveData.value = ResponseStatus(response.code(), response.message())
                }
            }
        })
    }

    fun deletePlaylist(token: String, id: Int) {
        val apiInterface = RestClient.getApiInterface(token)
        apiInterface.deletePlaylist(id).enqueue(object : Callback<Unit> {
            override fun onFailure(call: Call<Unit>?, t: Throwable?) {
            }

            override fun onResponse(call: Call<Unit>?, response: Response<Unit>?) {
                playlistDeleteLiveData.value = ResponseStatus(response!!.code(), response.message())
            }

        })
    }

}