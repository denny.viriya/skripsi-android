package com.edacity.edacitymikroskil.main

import android.content.BroadcastReceiver
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import com.edacity.edacitymikroskil.R
import com.google.firebase.analytics.FirebaseAnalytics
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.core.CrashlyticsCore
import com.edacity.edacitymikroskil.personal.reminder.ReminderManager
import io.fabric.sdk.android.Fabric




abstract class BaseActivity: AppCompatActivity() {

    private lateinit var mFirebaseAnalytics : FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initFirebase()
    }

    override fun finish() {
        super.finish()
        onLeaveThisActivity()
    }

    protected fun onLeaveThisActivity() {
        overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_right)
    }

    // It's cleaner to animate the start of new activities the same way.
    // Override startActivity(), and call *overridePendingTransition*
    // right after the super, so every single activity transaction will be animated:

    override fun startActivity(intent: Intent) {
        super.startActivity(intent)
        onStartNewActivity()
    }

    override fun startActivity(intent: Intent, options: Bundle?) {
        super.startActivity(intent, options)
        onStartNewActivity()
    }

    protected fun onStartNewActivity() {
        overridePendingTransition(R.anim.enter_from_right, R.anim.exit_to_left)
    }

    private fun initFirebase() {
        val crashlyticsKit = Crashlytics
                .Builder()
                .core(CrashlyticsCore.Builder().build())
                .build()
        Fabric.with(this, crashlyticsKit)
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this)
    }
}