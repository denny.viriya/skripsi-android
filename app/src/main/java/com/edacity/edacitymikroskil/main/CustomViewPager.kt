package com.edacity.edacitymikroskil.main

import android.content.Context
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent
import com.edacity.edacitymikroskil.homepage.HomeFragment
import com.edacity.edacitymikroskil.personal.PersonalFragment
import com.edacity.edacitymikroskil.playlist.PlaylistFragment
import com.edacity.edacitymikroskil.searchpage.SearchFragment

class CustomViewPager(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {
    var isPagingEnabled: Boolean = true

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (isPagingEnabled)
            super.onTouchEvent(event)
        else
            false
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return isPagingEnabled && super.onInterceptTouchEvent(event)
    }

    class PagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {
        private var tabItems = 4

        override fun getItem(pos: Int): Fragment {
            return when (pos) {
                0 -> HomeFragment.newInstance()

                1 -> SearchFragment.newInstance()

                2 -> PlaylistFragment.newInstance()

                3 -> PersonalFragment.newInstance()

                else -> HomeFragment.newInstance()
            }
        }

        override fun getCount(): Int {
            return tabItems
        }
    }
}