package com.edacity.edacitymikroskil.main

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import com.edacity.edacitymikroskil.homepage.HomeFragment

class MainViewModel(application: Application) : AndroidViewModel(application) {

    var currentPageLiveData : MutableLiveData<Int>? = null
        get() {
            if (field == null) {
                field = MutableLiveData()
            }
            return field
        }


    var onError : MutableLiveData<Throwable>? = null
        get() {
            if (field == null) field = MutableLiveData()
            return field
        }
        private set



}