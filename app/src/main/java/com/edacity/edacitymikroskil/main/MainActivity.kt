package com.edacity.edacitymikroskil.main

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.os.Message
import android.support.design.widget.BottomNavigationView
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.support.v7.widget.AppCompatTextView
import android.support.v7.widget.Toolbar
import android.view.MenuItem
import com.edacity.edacitymikroskil.R
import com.edacity.edacitymikroskil.service.MainService
import com.google.android.exoplayer2.util.Util

class MainActivity : BaseActivity() {
    private lateinit var mViewModel: MainViewModel
    private val INITIAL_FRAGMENT_PAGE = "page"
    private lateinit var mBottomNavigationView: BottomNavigationView
    private lateinit var mViewPager: CustomViewPager
    private lateinit var mToolbar: Toolbar
    private lateinit var mToolbarTitle: AppCompatTextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
        initObject()
        setupViewPager(CustomViewPager.PagerAdapter(supportFragmentManager))
        initListener()
    }

    private fun initView() {
        mToolbar = findViewById(R.id.include_toolbar)
        mToolbarTitle = mToolbar.findViewById(R.id.toolbar_title)
        setupToolbar(mToolbar)
        mBottomNavigationView = findViewById(R.id.bottom_navigation_view)
        mViewPager = findViewById(R.id.view_pager)
        BottomNavigation.disableShiftMode(mBottomNavigationView)
    }

    private fun setupToolbar(toolbar: Toolbar) {
        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayShowTitleEnabled(false)
    }

    private fun initObject() {
        mViewModel = ViewModelProviders.of(this).get(MainViewModel::class.java)

        mViewModel.currentPageLiveData!!.value = intent.getIntExtra(INITIAL_FRAGMENT_PAGE, 0)
        mViewModel.currentPageLiveData!!.observe(this, Observer {
            currentPage ->

            when (currentPage) {
                0 -> {
                    setMenu(currentPage)
                    mToolbarTitle.text = "Homepage"
                }
                1 -> {
                    setMenu(currentPage)
                    mToolbarTitle.text = "Search"
                }
                2 -> {
                    setMenu(currentPage)
                    mToolbarTitle.text = "Playlist"
                }
                3 -> {
                    setMenu(currentPage)
                    mToolbarTitle.text = "Personalize"
                }
            }
        })
    }

    private fun setupViewPager(adapter: PagerAdapter) {
        mViewPager.offscreenPageLimit = 4
        mViewPager.adapter = adapter
    }

    private fun  initListener() {
        mBottomNavigationView.setOnNavigationItemSelectedListener { item: MenuItem ->
            item.isChecked = true
            when (item.itemId) {
                R.id.home -> {
                    mViewModel.currentPageLiveData!!.value = 0
                }

                R.id.search -> {
                    mViewModel.currentPageLiveData!!.value = 1
                }

                R.id.playlist -> {
                    mViewModel.currentPageLiveData!!.value = 2
                }

                R.id.reminder -> {
                    mViewModel.currentPageLiveData!!.value = 3
                }
            }
            false
        }

        mViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(state: Int) {}

            override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {}

            override fun onPageSelected(position: Int) {
                mViewModel.currentPageLiveData!!.value = position
            }
        })
    }

    private fun setMenu(currentItem : Int){
        mViewPager.currentItem = currentItem
    }

}
